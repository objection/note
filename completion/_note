#compdef note

_note_complete () {

	# We pass the LBUFFER, which is the buffer to the left of the
	# cursor. The lovely thing about this is it means we don't have to
	# consider where the cursor position is at all. All Note will see
	# is the stuff that can actually be completed. The only small
	# downside to this is it means we can't try completing anything
	# after the cursor position, but I'm not really interested in
	# doing that anyway. Most completion systems don't do that. Zsh is
	# presumably very capable of doing that, since it is has PREFIX
	# (the part of the word before the cursor)
	# and SUFFIX, the part of the word after the cursor.

	# ${LBUFFER#* } replaces the the first word with nothing. "#"
	# does a replacement of the initial part of the variable. *
	# matches anything, of course, and the space matches a space.
	# Interestingly it also matches tabs. Doing this might have us end
	# up with a string like "           this that", but Note shouldn't
	# care about extra whitespace. Also, by the way, the "#" makes the
	# match non-greedy. "##" is greedy.
	echo "LBUFFER: \"$LBUFFER\"" > /tmp/lbuffer
	echo "words: \"$words \"" > /tmp/words
	echo \"${LBUFFER#* }\" > /tmp/other-lbuffer

	# LBUFFER includes quotes, by the way, so:
	# 	note "this that" this
	# will be
	# 	note "this that" this
	#
	# It not something like
	# 	note this that this
	#
	# So it's a string with the words preserved.
	words=($(note --complete="${LBUFFER#* }"))
	compadd -- "${(@)words}"
	return 0
}

_arguments -s -S "*::arg: _note_complete" --
