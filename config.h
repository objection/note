#pragma once

/* Default --newlines. */
#define NNEWLINES_BETWEEN_NOTES 0

/* Default --width. */
#define PRINT_MAX_COLS 80

#define DEFAULT_DATA_DIR "~/.local/share/note"

#define ANSI_BLACK 				"\x1b[30m"
#define ANSI_RED 				"\x1b[31m"
#define ANSI_GREEN 				"\x1b[32m"
#define ANSI_YELLOW 			"\x1b[33m"
#define ANSI_BLUE 				"\x1b[34m"
#define ANSI_MAGENTA 			"\x1b[35m"
#define ANSI_CYAN 				"\x1b[36m"
#define ANSI_WHITE 				"\x1b[37m"
#define ANSI_NO_COLOUR 			"\x1b[0m"
#define ANSI_BOLD 				"\x1b[1m"
#define ANSI_ITALIC     		"\x1b[3m"
#define ANSI_NO_ITALIC  		"\x1b[23m"
#define ANSI_BRIGHT_BLACK 		"\x1b[90m"
#define ANSI_BRIGHT_RED 		"\x1b[91m"
#define ANSI_BRIGHT_GREEN 		"\x1b[92m"
#define ANSI_BRIGHT_YELLOW	 	"\x1b[93m"
#define ANSI_BRIGHT_BLUE 		"\x1b[94m"
#define ANSI_BRIGHT_MAGENTA 	"\x1b[95m"
#define ANSI_BRIGHT_CYAN 		"\x1b[96m"
#define ANSI_BRIGHT_WHITE 		"\x1b[97m"
#define ANSI_BRIGHT_NO_COLOUR 	"\x1b[0m"

