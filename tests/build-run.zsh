#!/usr/bin/env zsh

cd $0:h
gcc -Wall -ggdb3 test.c -o test \
		-luseful -ldarr -ln-fs
if [[ $1 == -r ]]; then
	./test
fi
