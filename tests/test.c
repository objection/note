#if 0
#endif
#define _GNU_SOURCE
#include <n-script.h>

#define $note "../build/note"

struct args {
	struct strs strs;
} args;

error_t parse_opt (int key, char *arg, struct argp_state *state) {

	switch (key) {
		case ARGP_KEY_INIT:
			break;
		case ARGP_KEY_ARG:
			arr_push (&args.strs, arg);
			break;
		case ARGP_KEY_END:
			break;
		default: break;
	}
	return 0;
}

enum expected_result {
	ER_FAIL,
	ER_PASS,
};

int run (enum expected_result expected_result, char *fmt, ...) {
	assert (fmt);
	va_list ap;
	va_start (ap, fmt);
	char *buf;
	vasprintf (&buf, fmt, ap);
	assert (buf);
	// Always runs on ./test.nxt. This means it won't find bugs
	// in the notefile-selection code.
	printf ("\"" $note " f test.nxt %s\":\n", buf);
	int res = systemf ($note " f test.nxt %s", buf);
	if (res == -1 || res == 127)
		err (1, "system failed");
	else {
		if ((res && expected_result == ER_FAIL)
				|| (!res && expected_result == ER_PASS))
			printf ("\tCORRECTLY ");
		else
			printf ("\tINCORRECTLY ");
		if (expected_result == ER_FAIL)
			printf ("failed\n");
		else
			printf ("passed\n");
	}
	putchar ('\n');
	free (buf);
	va_end (ap);
	return res;
}

int main (int argc, char **argv) {
	struct argp argp = {
		.options = (struct argp_option[]) {
			{"arg", 'a', 0, 0,
				"Description"},
			{0},
		},
		parse_opt,
		"PARAMS ...", "\
Description ...\v\
Blah blah"
	};

	if (!$strmatch (basename (get_current_dir_name ()), "test")
			&& !fexists ("test.c"))
		err (1, "you don't appear to be in the test directory");

	argp_parse (&argp, argc, argv, 0, 0, NULL);

	remove ("test.nxt");
	run (ER_PASS, ",\"This and that\"");
	/* run (ER_PASS, "n-1 ,\"That and this\""); */
	/* run (ER_FAIL, "n-1 /KSDJFLSDJKFLSDJKFLJSDFJ/"); */

	exit (0);
}



