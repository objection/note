#define _GNU_SOURCE

#include "str.h"
#include "cmp.h"
#include "macros.h"
#include <string.h>
#include <ctype.h>
#include <regex.h>
#include <darr.h>

bool intnmatch (int *a, int *b, size_t n) {
	for (int *p = a, *q = b; p < a + n; p++)
		if (*p != *q)
			return 0;
	return 1;
}

int count_occurences_of_chars (char *str, char the_char) {
	int r = 0;
	for (char *p = str; *p; p++) {
		if (*p == the_char)
			r++;
	}
	return r;
}


void strip (char *s) {
	char *p = s;
	int len = strlen (p);
	if (len == 0) return;

	while (isspace (p[len - 1])) p[--len] = 0;
	while (*p && isspace (*p)) ++p, --len;

	memmove (s, p, len + 1);
}

bool str_arr_is_equal (str_arr_s *a, str_arr_s *b) {
	if (a->n != b->n) return 0;
	FORI (i, a->n) {
		if (!!strcmp (a->d[i], b->d[i]))
			return 1;
	}
	return 0;
}

int n_matching_initial_chars (char *against, char *match, int n_match) {
	int r = -1;
	FORI (i, n_match) {
		if (against[i] != match[i])
			break;
		r++;
	}
	return r;
}

int get_index_of_longest_unambiguous_initial_match (char *str, char **strs, int n_strs) {

	int matching_lens[n_strs];

	// I think the whole thing could be done in one loop.
	FORI (i, n_strs)
		matching_lens[i] = n_matching_initial_chars (strs[i], str, strlen (str));

	int max = -1, longest = -1;
	FORI (i, n_strs) {
		if (matching_lens[i] > max) {
			max = matching_lens[i];
			longest = i;
		}
	}
	qsort (matching_lens, n_strs, sizeof *matching_lens, cmp_int_cb);
	FORI (i, n_strs) {
		if (i < n_strs - 1 && matching_lens[i] == matching_lens[i + 1]
				&& matching_lens[i] == max)
			return -1;
	}
	return longest;
}

// Assumes you've passed a msg_buf of at least 0xff
// This needs to go in a library somewhere, but where?
// I haven't tested this. And I'm not sure to make it work reliably
int get_regmatching_members (const char *pattern, ssize_t n_objs,
		void *objs, ssize_t obj_size, ssize_t member_offset, char *msg_buf,
		struct partiallymatched_ops *out) {
	int rt = 0;
	regex_t preg = {};
	memset (out, 0, sizeof *out);

	int r = 1;
	if ((rt = regcomp (&preg, pattern, REG_EXTENDED | REG_NOSUB))) {

		regerror (rt, &preg, msg_buf, 0xff);
		goto out;
	} else {
		FORI (i, n_objs) {

			// Look at this. It does have to be a char **, because
			// strings are char *s, themselves.
			char **offset = (objs + (i * obj_size)) + member_offset;
			if (!offset) continue;

			// Clang complains about *offset, here. It says it's
			// uninitialised. I _could_ be uninitialised, but
			// there's nothing I can do about here, right?
			if (REG_NOMATCH == (rt = regexec (&preg, *offset, 0, 0, 0)))
				goto out;
			else if (!rt)
				arr_add (out, i);
		}
	}
	r = 0;
out:
	return r;
}

