#pragma once
#include "tui.h"
#include "../macros.h"
#include <libnote/libnote.h>

enum {
	MAX_KEY_DESCRIPTION = 17,
	MAX_OP_KEYS = 8,
	MAX_KEYS_AS_STR_LEN = MAX_OP_KEYS * 2,
};

#define PUSH_NO_ARGS_OP($op_arr, $short_description, $fn, ...) do { \
	int n_keys = countof (((int []) {__VA_ARGS__})); \
	push_op ($op_arr, $short_description, n_keys, (int []) {__VA_ARGS__}, 0, \
			nullptr, $fn); \
} while (0);
#define PUSH_OP_WITH_ARGS($op_arr, $short_description, $args, $fn, ...) do { \
	int n_keys = countof (((int []) {__VA_ARGS__})); \
	int n_args = countof ($args); \
	push_op ($op_arr, $short_description, n_keys, (int []) {__VA_ARGS__}, \
			n_args, ($args), $fn); \
} while (0);

// These are unique no to ops. They always get items, statments, etc.
union op_args {
	int i;
	int b;
};

// What I'm doing here is probably a disgrace. This macro is for a bunch of
// function pointers, and, of course, not all of the parameters will be used.
// So, it seems I can just say they're all unused. All the attribute seems
// to do is make gcc note warn if it's unused. So it's fine.
#define MAKE_OP_FUNC(name) \
	int op_##name ( \
			[[maybe_unused]] union op_args op_args[3], \
			[[maybe_unused]] struct tui *t, \
			[[maybe_unused]] struct widget *w)
typedef MAKE_OP_FUNC (fn);

#define MAKE_DEFAULT_OP($name) \
	void default_op_##$name ( \
			[[maybe_unused]] struct tui *t, \
			[[maybe_unused]] struct widget *w, \
			[[maybe_unused]] int ch)
typedef MAKE_DEFAULT_OP (fn);

typedef struct op op;
struct op {
	int keys[MAX_OP_KEYS];

	// Just pre-compute this; it won't change. This is something like
	// "^B^A", four character, not 2, as it would be in a string
	// because it's two control characters.
	char keys_as_str[MAX_KEYS_AS_STR_LEN];
	int keys_as_str_len;
	short n_keys;
	char *short_description;
	op_fn *fn;
	union op_args args[3];
};

// You could make default ops the same as normal ops, particularly if
// you decide that it's useful to pass the character that got you
// there to op funcs. But for now let's make it a different struct.
typedef struct default_op default_op;
struct default_op {
	char *short_description;
	default_op_fn *fn;
};

typedef struct op_arr op_arr;
$make_arr (struct op, op_arr);


typedef struct widget_op_mode widget_op_mode;
struct widget_op_mode {
	op_arr ops;

	// You can use this for, eg (as I am, here) exiting the cmdline
	// popup mode when you type something that's not a valid popup
	// key, ie, a character, you've continued to type.
	default_op default_op;
};

typedef struct widget_op_modes widget_op_modes;
struct widget_op_modes {
	widget_op_mode *d;
	int n;
};

enum widget_flags {
	WIDGET_FLAG_REDRAW = 1 << 0,
};

typedef struct widget widget;
struct widget {

	// Needs to be a pointer; it's a widget. This means it basically
	// needs to be malloced, too, else you wouldn't be able to make
	// multiple of that widget with separate statuslines.
	//
	// Also, the draw_statusline function clears the screen and sets
	// it to reverse video.
	struct widget *statusline_widget;

	bool is_popup;

	// Used for the completion popup. It's not necessary to do this
	// since completion is only used in the cmdline widget but this is
	// the more proper way to do it. The thing is that though the
	// completion widget is a widget, a lot of what it mucks about
	// with is the cmdline widget.
	widget *parent_widget;

	WINDOW *win;
	vec pad_tl;

	// This gets filled in with the actual dimensions of the window as
	// they're erawn. Maybe, maybe these shouldn't be here since
	// they're ephemeral. They're used in drawn and shouldn't be used
	// elsewhere.
	rect screen_rect;

	// Some widgets must be a particular size, like the statusline.
	// Some don't, like the items; they expand to fit. If either of
	// the x or y are 0 this it means "expand to fit".
	vec required_screen_wh;

	vec specified_tl;

	vec cursor;
	vec last_cursor;
	enum widget_flags flags;
	struct displayed_elem_normal_mode_fns *fns;

	widget_op_modes modes;

	widget_op_mode *(*get_op_mode) (struct widget *);

	char *name;
	char *description;

	void (*draw) (tui *, widget *, int);
};

struct __widgets_struct {
	widget items;
	widget cmdline;
	widget cmdline_window;
	widget multi_confirm;
	widget completion;
	widget keys;
	widget msg;
	widget input;
};

typedef struct widgets widgets;
struct widgets {
	widget *current;
	union {
		struct __widgets_struct as;
		widget arr[sizeof (struct __widgets_struct) / sizeof (struct widget)];
	};
};

typedef struct displayed_elem_normal_mode_fns displayed_elem_normal_mode_fns;
struct displayed_elem_normal_mode_fns {
	int (*get_idx_of_first_element) (struct tui *, widget *);
	int (*get_element_count) (struct tui *, widget *);
	struct vec (*cursor_y_from_pad_line) (struct tui *, int);
	vec (*elem_tl_from_cursor) (struct tui *, struct vec);
	vec (*elem_br_from_cursor) (struct tui  *, struct vec);

	// Think of a better name. This *is* a generic function. You
	// should be able to use it in the cmdline window, and, I suppose,
	// the multi-confimation one.
	struct tsk_item *(*elem_from_cursor) (struct tui *, struct widget *);
};

static inline widget *cur_widget (tui *t) {
	return t->widgets->current;
}

struct widgets *set_up_widgets (tui *t);
struct op_arr set_up_ops (char which);

void highlight_selected_item (struct tui *t, struct widget *w, enum tsk_colour colour);
void increment_cursor (tui *t, widget *w);
void decrement_cursor (tui *t, widget *w);
void push_op (struct op_arr *op_arr, char *short_description, int n_keys, int *keys,
		int n_args, union op_args args[n_args], op_fn *fn);
void go_to_a_cmdline (tui *t, widget *cmdline_widget, enum editbuf_id editbuf_id);
void make_new_cmdline_and_go_to_it (tui *t, enum editbuf_id editbuf_id, bool forwards);
void insert_word_into_editbuf (struct editbuf *editbuf, int completion_word_start,
		char *word, widget *w);
void refresh_widget (widget *w);
void resize (struct tui *t);
void erase_widgets (struct widgets *widgets);
void go_to_parent_widget (tui *t, widget *w);
widget_op_modes make_widget_op_modes (int n_widget_mode_arr, widget_op_mode *widget_mode_arr);
default_op make_default_op (default_op_fn *fn, char *short_description);
void draw_cmdlinelike_widget (struct tui *t, widget *w, char *chars_to_print_at_left);
