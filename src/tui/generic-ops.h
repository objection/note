#pragma once

#include "widget.h"

MAKE_OP_FUNC (G);
MAKE_OP_FUNC (c_b);
MAKE_OP_FUNC (c_d);
MAKE_OP_FUNC (c_e);
MAKE_OP_FUNC (c_f);
MAKE_OP_FUNC (c_u);
MAKE_OP_FUNC (c_y);
MAKE_OP_FUNC (change_mode_to_cmdline);
MAKE_OP_FUNC (change_mode_to_cmdline_win);
MAKE_OP_FUNC (change_mode_to_normal);
MAKE_OP_FUNC (decrement_cursor);
MAKE_OP_FUNC (gg);
MAKE_OP_FUNC (increment_cursor);
MAKE_OP_FUNC (redraw);
MAKE_OP_FUNC (search);
MAKE_OP_FUNC (go_to_parent_widget);
MAKE_OP_FUNC (toggle_keys);
MAKE_OP_FUNC (delete_to_start);
MAKE_OP_FUNC (delete);
MAKE_OP_FUNC (delete_word_behind);
MAKE_DEFAULT_OP (self_insert);
void push_generic_screenmoving_ops (struct op_arr *out);
