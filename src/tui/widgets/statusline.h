#pragma once

#include "../widget.h"

widget make_statusline_widget (tui *, char *name, void (*draw) (tui *, widget *));
