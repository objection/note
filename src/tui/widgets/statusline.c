#include "../widget.h"

widget make_statusline_widget (tui *, char *name, void (*draw) (tui *, widget *, int)) {
	return (widget) {
		.name = name,
		.required_screen_wh = (vec) {.y = 1},
		.draw = draw,
	};
}
