#include "../cmd.h"
#include "../generic-ops.h"
#include "../tui.h"
#include "statusline.h"
#include <ctype.h>
#include <rab.h>

static int toggle_any_prop (tui *t, widget *w, int prop, size_t offset) {
	auto item = w->fns->elem_from_cursor (t, w);
	char *p_offset = (char *) item + offset;
	int *the_flag = (int *) p_offset;
	*the_flag ^= (1 << prop);
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return 0;
}

static struct tsk_strs get_recalculate_view_words (tui *t) {

	struct tsk_strs r = {};
	auto words = &(cur_cmd (t))->words;
	arr_each (words, _) {

		// This is flimsy, of course. I'm in a funny mood, so I'm
		// making myself laugh. This works (for now) because cmds
		// start with uppercase letters and you can repeat them.
		char *word_start = rab_skip_space (*_);
		if (isupper (word_start[0]))
			arr_add (&r, strdup ("Print"));
		else
			arr_add (&r, strdup (*_));
	}
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return r;
}

static MAKE_OP_FUNC (normal_prev_view) {
	*(struct vec *) cur_cmd (t)->user = t->widgets->as.items.cursor;
	if (t->cmds.idx)
		t->cmds.idx--;
	t->widgets->as.items.cursor = *(struct vec *) (cur_cmd (t))->user;
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return 0;
}

static MAKE_OP_FUNC (normal_next_view) {
	*(struct vec *) cur_cmd (t)->user = t->widgets->as.items.cursor;
	if (t->cmds.idx < (int) t->cmds.n - 1)
		t->cmds.idx++;
	t->widgets->as.items.cursor = *(struct vec *) (cur_cmd (t))->user;
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return 0;
}

static struct tsk_item *item_from_cursor (struct tui *t,
		struct widget *w) {
	return t->tsk_ctx.items.d + t->tui_item_view.d[w->cursor.y].item_idx;
}

static struct vec cursor_y_from_pad_line (struct tui *t, int pad_line) {
	auto tui_item_view = &t->tui_item_view;

	// This is bad, I think. Or is it?
	struct vec r = t->widgets->as.items.cursor;
	arr_each (tui_item_view, _) {
		if (_->rect.tl.y >= pad_line) {
			r.y = pad_line = _ - tui_item_view->d;
			break;
		}
	}
	return r;
}

static vec elem_tl_from_cursor (struct tui *t, struct vec cursor) {
	if (!t->tui_item_view.d)
		return (vec) {};
	return t->tui_item_view.d[cursor.y].rect.tl;
}

static vec elem_br_from_cursor (struct tui *t, struct vec cursor) {
	if (!t->tui_item_view.d)
		return (vec) {};
	return t->tui_item_view.d[cursor.y].rect.br;
}

static int get_idx_of_first_element (tui *, widget *) {
	return 0;
}

static int get_element_count (tui *t, widget *) {
	return t->tui_item_view.n;
}

static MAKE_OP_FUNC (edit_in_editor) {
	if (!t->tui_item_view.n) {
		print_err (t, "No items to edit");
		return 0;
	}
	int elem_idx = t->tui_item_view.d[(w)->cursor.y].item_idx;

	// This endwin and the doupdate seem to be necessary
	// From man endwin:
	// 		A program  should  always call endwin before exiting or
	// 			escaping from curses mode temporarily.
	// And then it says that does this that and the other.
	// And about doupdate:
	// 		Calling refresh(3X) or doupdate(3X) after a temporary
	// 			escape causes the program to resume visual mode.
	// As for why this is the case, I have no idea.
	endwin ();
	tsk_change_in_editor (&t->tsk_ctx, cur_cmd (t),
			&(tsk_view) {.d = &(view_node) {.item_idx = elem_idx}, .n = 1},
			cur_statement (t),
			(struct mod_piece) {.cmd_words_idx = AE_NO_CMD_WORD_TO_PRINT});
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	doupdate ();
	return 0;
}


static MAKE_OP_FUNC (toggle_done) {
	toggle_any_prop (t, w, NP_DONE, offsetof (struct tsk_item, has));
	return 0;
}

static MAKE_OP_FUNC (toggle_remove) {
	toggle_any_prop (t, w, IP_REMOVE, offsetof (struct tsk_item, has));
	return 0;
}

static MAKE_OP_FUNC (toggle_locked) {
	toggle_any_prop (t, w, IP_LOCK, offsetof (struct tsk_item, has));
	return 0;
}

static MAKE_OP_FUNC (recalc_view_make_new) {
	auto words = get_recalculate_view_words (t);
	tui_do_cmd (t, &words, 0);
	arr_each (&words, _)
		free (*_);
	free (words.d);
	return 0;
}

static MAKE_OP_FUNC (recalc_view_replace) {
	auto words = get_recalculate_view_words (t);
	tui_do_cmd (t, &words, 1);
	arr_each (&words, _)
		free (*_);
	free (words.d);
	return 0;
}

static MAKE_OP_FUNC (add_item) {

	// Necessary, and so is the doupdate below.
	endwin ();
	struct tsk_strs cmd_words = {};
	arr_add (&cmd_words, ":edit");

	tui_do_cmd (t, &cmd_words, false);

	free (cmd_words.d);
	doupdate ();
	return 0;
}

static MAKE_OP_FUNC (toggle_justification) {
	t->keys_justification = !t->keys_justification;
	return 0;
}

static widget_op_modes make_op_modes (void) {
	struct op_arr ops = {};

	PUSH_NO_ARGS_OP (&ops, "Toggle this",      op_toggle_keys,          'K');
	PUSH_NO_ARGS_OP (&ops, "Go to first item", op_gg,                   'g', 'g');
	PUSH_NO_ARGS_OP (&ops, "Go to last item",  op_G,                    'G');
	push_generic_screenmoving_ops (&ops);

	PUSH_NO_ARGS_OP (&ops, "Cursor up",        op_increment_cursor,     'j');
	PUSH_NO_ARGS_OP (&ops, "Cursor down",      op_decrement_cursor,     'k');
	PUSH_NO_ARGS_OP (&ops, "Add item",         op_add_item,             'a');
	PUSH_NO_ARGS_OP (&ops, "Edit note",        op_edit_in_editor,       'i');
	PUSH_NO_ARGS_OP (&ops, "Edit note",        op_edit_in_editor,       '\r');
	PUSH_NO_ARGS_OP (&ops, "Toggle done",      op_toggle_done,          'd');
	PUSH_NO_ARGS_OP (&ops, "Toggle remove",    op_toggle_remove,        'r');
	PUSH_NO_ARGS_OP (&ops, "Toggle locked",    op_toggle_locked,        'l');
	PUSH_NO_ARGS_OP (&ops, "Redraw",           op_redraw,               TUI_CTRL ('l'));
	PUSH_NO_ARGS_OP (&ops, "Recalc view",      op_recalc_view_make_new, 'R');
	PUSH_NO_ARGS_OP (&ops, "Recalc repl view", op_recalc_view_replace,  TUI_CTRL ('R'));
	PUSH_NO_ARGS_OP (&ops, "Prev view",        op_normal_prev_view,     TUI_CTRL ('o'));
	PUSH_NO_ARGS_OP (&ops, "Next view",        op_normal_next_view,     TUI_CTRL ('i'));
	PUSH_NO_ARGS_OP (&ops, "Next match",       op_search,               'n');
	PUSH_NO_ARGS_OP (&ops, "Justify keys",     op_toggle_justification, TUI_CTRL ('j'));

	PUSH_OP_WITH_ARGS (&ops, "Search forwards",
			((union op_args []) {{.i = EID_SEARCH}, {.b = true}}),
			op_change_mode_to_cmdline, '/');
	PUSH_OP_WITH_ARGS (&ops, "Search backwards",
			((union op_args []) {{.i = EID_SEARCH}, {.b = false}}),
			op_change_mode_to_cmdline, '?');
	PUSH_OP_WITH_ARGS (&ops, "Enter cmd",
			((union op_args []) {{.i = EID_CMD}, {.b = false}}),
			op_change_mode_to_cmdline, ':');
	PUSH_OP_WITH_ARGS (&ops, "Prev match",
			((union op_args []) {{.b = true}}),
			op_search, 'N');

	return make_widget_op_modes (1, &(widget_op_mode) {.ops = ops});
}

// This function gets the printed notes from tsk_print and displays
// it.
static void draw (struct tui *t, widget *w, int) {

	auto cmd = cur_cmd (t);

	// Here we do the drawing if we need to. It belatedly occurs to me
	// that it's really dumb to do it here. We should do it on the
	// spot when we change the items. Same goes for all of the drawing
	// functions.
	if (t->flags & TF_RECREATE_WINDOW_CONTENTS) {

		t->tui_item_view.n = 0;
		werase (w->win);

		arr_each (&cmd->statements, statement) {
			arr_each (statement->view, _) {

				int s = getcury (w->win);
				tsk_print (&t->tsk_ctx, cmd, (view_node) {.item_idx = _->item_idx},
						_ - statement->view->d, false,
						statement->action.mods.print_what.val.prop_id ?:
							t->tsk_ctx.opts->print_what);
				int e = getcury (w->win);
				arr_add (&t->tui_item_view, ((struct tui_view_elem) {
							.item_idx = _->item_idx,
							.rect = {.tl = {.y = s}, .br = {.y = e}}
						}));
			}
		}
	}
	highlight_selected_item (t, w, TSK_COLOUR_PRINT_HEADER);
}

static void draw_statusline (struct tui *t, widget *w) {

	auto cmd = cur_cmd (t);

	if (t->widgets->current == &t->widgets->as.cmdline_window)
		mvwaddstr (w->win, 0, 0, "[Cmdline]");
	else {

		if (cmd->view_words.n)  {
			wmove (w->win, 0, 0);
			wprintw (w->win, "View: %s", cmd->view_words.d[0]);
			for (ssize_t i = 1; i < cmd->view_words.n; i++)
				wprintw (w->win, " %s", cmd->view_words.d[i]);

		} else // This should actually be impossible.
			mvwprintw (w->win, 0, 0, "[No view]");
	}
}

widget make_items_widget (tui *t) {

	static displayed_elem_normal_mode_fns displayed_fns = {
		.get_idx_of_first_element = get_idx_of_first_element,
		.get_element_count        = get_element_count,
		.cursor_y_from_pad_line   = cursor_y_from_pad_line,
		.elem_tl_from_cursor      = elem_tl_from_cursor,
		.elem_br_from_cursor      = elem_br_from_cursor,
		.elem_from_cursor         = item_from_cursor,
	};

	widget *statusline_widget = malloc (sizeof *statusline_widget);
	*statusline_widget = make_statusline_widget (t, "items statusline", draw_statusline);

	auto r = (widget) {
		.name = "items",
		.description = "Scroll through items and perform ops on them",
		.fns =

			// Maybe later we could use constexpr and define this
			// directly here.
			&displayed_fns,
		.modes = make_op_modes (),
		.draw = draw,
		.statusline_widget = statusline_widget,
	};

	return r;
}

