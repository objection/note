#define _GNU_SOURCE

#include "keys.h"
#include "statusline.h"
#include "../../macros.h"

enum {
	SPACE_BETWEEN_ENTRIES = 3,
};
// We're making a bespoke data structure here to do something
// straightforward: count up the "key output strs" (something like
// "%B: Scroll screen", get their maxes so we can print them.

enum {
	MAX_KEY_OUTPUT_STR = 32,
	MAX_KEYS = 64,
};

typedef struct key_output_str key_output_str;
struct key_output_str {
	char str[MAX_KEY_OUTPUT_STR];
	int len;
};

typedef struct key_output_strs key_output_strs;
struct key_output_strs {
	struct key_output_str d[MAX_KEYS];
	int n;
	int max;
	int max_key_chain;
};

static void set_key_output_str (op *op, int max_key_chain_str_len,
		enum keys_justification justification,
		key_output_str *out) {

		out->len = 0;
		out->len += snprintf (out->str, sizeof out->str, "%s:", op->keys_as_str);

		int lhs_len = max_key_chain_str_len

			// For the colon.
			+ 1;
		if (justification == KJ_LEFT) {
			for (; out->len < lhs_len; out->len++)
				out->str[out->len] = ' ';
		}

		// Add an extra space, between the ":" and the start of the
		// descr.
		out->str[out->len++] = ' ';
		int remaining = sizeof out->str - out->len;
		out->len += snprintf (out->str + out->len, remaining, "%s",
				op->short_description);
		ASSERT (out->len < (int) countof (out->str));
}

static void append_to_key_output_strs (op *op, int max_key_chain_str_len,
		enum keys_justification justification, key_output_strs *key_output_strs) {
	ASSERT (key_output_strs->n < MAX_KEYS);
	auto key_output_str = &key_output_strs->d[key_output_strs->n];
	set_key_output_str (op, max_key_chain_str_len, justification, key_output_str);
	UPDATE_MAX (&key_output_strs->max, key_output_str->len);
	key_output_strs->n++;
}

static void draw (struct tui *t, widget *w, int) {

	key_output_strs key_output_strs = {};

	wmove (w->win, 0, 0);
	werase (w->win);

	auto current_w = t->widgets->current;
	widget_op_mode *current_widget_op_mode = current_w->get_op_mode (current_w);

	// Get max key chain str len so we can line up, so it's like:
	//
	// 		a:  blah
	// 		ab: blah
	int max_key_chain_str_len = 0;
	if (t->partiallymatched_ops.n == 0) {
		EACH (op, current_widget_op_mode->ops.n, current_widget_op_mode->ops.d)
			UPDATE_MAX (&max_key_chain_str_len, op->keys_as_str_len);
	} else {
		auto op_idxes = &t->partiallymatched_ops;
		EACH (op_idx, op_idxes->n, op_idxes->d)
			UPDATE_MAX (&max_key_chain_str_len,
					current_widget_op_mode->ops.d[*op_idx].keys_as_str_len);
	}

	// Make the "key_output_strs" stuff; ultimately this is about
	// constructing strings for displaying and getting the length of
	// the biggest one so we can line them up.
	if (t->partiallymatched_ops.n == 0) {
		EACH (op, current_widget_op_mode->ops.n, current_widget_op_mode->ops.d) {
			append_to_key_output_strs (op, max_key_chain_str_len, t->keys_justification,
					&key_output_strs);
		}
	} else {
		auto op_idxes = &t->partiallymatched_ops;
		EACH (op_idx, op_idxes->n, op_idxes->d)
			append_to_key_output_strs (current_widget_op_mode->ops.d + *op_idx, max_key_chain_str_len,
					t->keys_justification, &key_output_strs);
	}

	char fmt_str[32];
	snprintf (fmt_str, sizeof fmt_str,

			t->keys_justification == KJ_RIGHT ?  "%%%ds" : "%%-%ds",

			key_output_strs.max + SPACE_BETWEEN_ENTRIES);

	int y, x;
	for (int i = 0; i < key_output_strs.n; i++) {
		getyx (w->win, y, x);

		// We have a print function (in tui.c) that will resize the
		// pad as we go, but we need to resize the pad ourselves here
		// because the wmove below won't go actually move the cursor
		// if there's nowhere to go.
		maybe_resize_pad_to_fit_new_lines (w->win, 1);
		if (x + key_output_strs.max > COLS - 1)
			wmove (w->win, (y + 1), 0);

		wprintw (w->win, fmt_str, key_output_strs.d[i].str);
	}
	if (current_widget_op_mode->default_op.short_description) {
		ASSERT (current_widget_op_mode->default_op.fn);
		if (key_output_strs.n) {
			y++;
			mv_print_fmt (w->win, y, 0, "Any other key: %s",
					current_widget_op_mode->default_op.short_description);
		} else
			mv_print_fmt (w->win, y, 0, "Any key: %s",
					current_widget_op_mode->default_op.short_description);

	}
	getyx (w->win, w->required_screen_wh.y, w->required_screen_wh.x);
	w->required_screen_wh.y++;
}

static void draw_statusline (tui *t, widget *w) {
	mv_print_fmt (w->win, 0, 0, "%s", t->widgets->current->description);
}

widget make_keys_widget (tui *t) {
	widget *statusline_widget = malloc (sizeof *statusline_widget);
	*statusline_widget = make_statusline_widget (t, "key widget statusline",
			draw_statusline);
	return (widget) {
		.name = "keys",
		.description = "This should be never ever seen",
		.required_screen_wh = (vec) {.y = 3},
		.draw = draw,
		.statusline_widget = statusline_widget,
	};
}
