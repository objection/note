#pragma once

#include "../widget.h"
widget make_completion_widget (tui *t);
int get_compl_required_screen_width (int n_str_lens, int str_lens[n_str_lens]);
