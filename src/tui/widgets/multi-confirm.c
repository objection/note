#include "multi-confirm.h"
#include "../generic-ops.h"
#include "../tui.h"

static vec elem_tl_from_cursor (struct tui *t, struct vec cursor) {

	// This is a hack. Because we do fix_view before we do
	// draw_confirmations, and because the user member is actually
	// created in draw_confirmations, it'll be null when we first
	// enter and will of course crash the program. Obviously the
	// confirmations should be properly set when we switch instead of
	// doing this.
	auto confirmation = &t->confirmations->d[cursor.y];
	if (!confirmation->user)
		return (vec) {};
	return ((struct rect *) confirmation->user)->tl;
}

static vec elem_br_from_cursor (struct tui *t, struct vec cursor) {
	auto confirmation = &t->confirmations->d[cursor.y];
	if (!confirmation->user)
		return (vec) {};
	return ((struct rect *) confirmation->user)->br;
}

static int get_element_count (tui *t, widget *) {
	return t->confirmations->n;
}

static int get_idx_of_first_element (tui *, widget *) {
	return 0;
}

// This function is basically identical to items_cursor_y_from_pad_line.
// They all need to go away, be replaced with lower-level functions
// that this kind of function call. Really all were need are functions
// to tell you the sizes of things.
static struct vec cursor_y_from_pad_line (struct tui *t, int pad_line) {
	struct vec r = {};
	arr_each (t->confirmations, _) {
		struct rect *rect = _->user;
		if (rect->tl.y > pad_line) {
			r.y = pad_line = _ - t->confirmations->d;
			break;
		}
	}
	return r;
}

static MAKE_OP_FUNC (yes) {
	t->confirmations->d[w->cursor.y].user_confirms = 1;
	return 0;
}

static MAKE_OP_FUNC (no) {
	t->confirmations->d[w->cursor.y].user_confirms = 0;
	return 0;
}

static MAKE_OP_FUNC (yes_to_all) {
	for (int i = w->cursor.y; i < (int) t->confirmations->n; i++)
		t->confirmations->d[i].user_confirms = 1;
	return 0;
}

static MAKE_OP_FUNC (no_to_all) {
	for (int i = w->cursor.y; i < (int) t->confirmations->n; i++)
		t->confirmations->d[i].user_confirms = 0;
	return 0;
}

static MAKE_OP_FUNC (reset) {
	arr_each (t->confirmations, _)
		_->user_confirms = 0;
	return 0;
}

static MAKE_OP_FUNC (set_all) {
	arr_each (t->confirmations, _)
		_->user_confirms = 1;
	return 0;
}

static MAKE_OP_FUNC (quit) {
	t->break_out = true;
	/* exit (0); */
	return 0;
}

static MAKE_OP_FUNC (ok) {
	t->break_out = true;
	return 0;
}

static widget_op_modes make_op_modes (void) {
	struct op_arr ops = {};

	PUSH_NO_ARGS_OP (&ops, "Toggle this",       op_toggle_keys,      'K');
	PUSH_NO_ARGS_OP (&ops, "Down",              op_c_e,              TUI_CTRL ('e'));
	PUSH_NO_ARGS_OP (&ops, "Up 1 line",         op_c_y,              TUI_CTRL ('y'));
	PUSH_NO_ARGS_OP (&ops, "Up 1 page",         op_c_b,              TUI_CTRL ('b'));
	PUSH_NO_ARGS_OP (&ops, "Down 1 page",       op_c_f,              TUI_CTRL ('f'));
	PUSH_NO_ARGS_OP (&ops, "Up half page",      op_c_u,              TUI_CTRL ('u'));
	PUSH_NO_ARGS_OP (&ops, "Down half page",    op_c_d,              TUI_CTRL ('d'));
	PUSH_NO_ARGS_OP (&ops, "Cursor up",         op_increment_cursor, 'j');
	PUSH_NO_ARGS_OP (&ops, "Cursor down",       op_decrement_cursor, 'k');
	PUSH_NO_ARGS_OP (&ops, "Go to first item",  op_gg,               'g', 'g');
	PUSH_NO_ARGS_OP (&ops, "Go to last item",   op_G,                'G');
	PUSH_NO_ARGS_OP (&ops, "Yes",               op_yes,              'y');
	PUSH_NO_ARGS_OP (&ops, "No",                op_no,               'n');
	PUSH_NO_ARGS_OP (&ops, "Yes to all below",  op_yes_to_all,       'Y');
	PUSH_NO_ARGS_OP (&ops, "No to all below",   op_no_to_all,        'N');
	PUSH_NO_ARGS_OP (&ops, "No to all",         op_reset,            'R');
	PUSH_NO_ARGS_OP (&ops, "Yes to all",        op_set_all,          'a');
	PUSH_NO_ARGS_OP (&ops, "Cancel confirms",   op_quit,             'q');
	PUSH_NO_ARGS_OP (&ops, "Accept choices",    op_ok,               'o');

	// Everything below is copy-pasted from items-widget; searching,
	// etc, could be a generic thing in all "normal modes".
	PUSH_NO_ARGS_OP (&ops, "Next match",        op_search,           'n');

	PUSH_OP_WITH_ARGS (&ops, "Search forwards",
			((union op_args []) {{.i = EID_SEARCH}, {.b = true}}),
			op_change_mode_to_cmdline, '/');
	PUSH_OP_WITH_ARGS (&ops, "Search backwards",
			((union op_args []) {{.i = EID_SEARCH}, {.b = false}}),
			op_change_mode_to_cmdline, '?');
	PUSH_OP_WITH_ARGS (&ops, "Enter cmd",
			((union op_args []) {{.b = true}}),
			op_search, 'N');
	return make_widget_op_modes (1, &(widget_op_mode) {.ops = ops});
}

static void draw (struct tui *t, widget *w, int) {
	if (!(t->flags & TF_RECREATE_WINDOW_CONTENTS))
		return;
	werase (w->win);
	auto confirmations = t->confirmations;
	arr_each (confirmations, _) {
		_->user = calloc (1, sizeof (struct rect));
		struct rect *rect = _->user;
		getyx (w->win, rect->tl.y, rect->tl.x);
		tsk_print_confirmation (&t->tsk_ctx, cur_cmd (t), _, tui_tsk_print);
		getyx (w->win, rect->br.y, rect->br.x);
	}
	highlight_selected_item (t, w, TSK_COLOUR_ERROR_HEADER);
}

widget make_multi_confirm_widget (tui *) {

	static displayed_elem_normal_mode_fns displayed_fns = {
		.get_idx_of_first_element = get_idx_of_first_element,
		.get_element_count        = get_element_count,
		.elem_tl_from_cursor      = elem_tl_from_cursor,
		.elem_br_from_cursor      = elem_br_from_cursor,
		.cursor_y_from_pad_line   = cursor_y_from_pad_line,
		.elem_from_cursor         = 0,
	};

	return (widget) {
		.name = "multi-confirm",
		.description = "Confirm actions or choose not to",
		.fns = &displayed_fns,
		.modes = make_op_modes (),
		.draw = draw,
	};
};

