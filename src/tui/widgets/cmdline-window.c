#define _GNU_SOURCE
#include "cmdline-window.h"
#include "../generic-ops.h"
#include "../../macros.h"

static int get_idx_of_first_element (tui *, widget *) {
	return 0;
}

static int get_element_count (tui *, widget *) {
	ASSERT (false && "NOT IMPLEMENTED");
	return 0;
}

static vec elem_tl_from_cursor (struct tui *, struct vec cursor) {
	return cursor;
}

static vec elem_br_from_cursor (struct tui *, struct vec cursor) {
	return cursor;
}

static struct vec cursor_y_from_pad_line (struct tui *, int line) {
	return (struct vec) {.y = line};
}

static widget_op_modes make_op_modes (void) {
	struct op_arr ops = {};

	push_generic_screenmoving_ops (&ops);
	PUSH_NO_ARGS_OP (&ops, "Toggle this",   op_toggle_keys,            'K');
	PUSH_NO_ARGS_OP (&ops, "Go to cmdline", op_change_mode_to_cmdline, TUI_CTRL ('c'));
	PUSH_NO_ARGS_OP (&ops, "Go to cmdline", op_change_mode_to_cmdline, ':');

	return make_widget_op_modes (1, &(widget_op_mode) {.ops = ops});
}

static void draw (struct tui *t, widget *w, int) {
	werase (w->win);
	wmove (w->win, 0, 0);
	auto editbuf = cur_editbuf (t);
	auto prevs = editbuf->prevs;

	for (int line = t->n_cmd_history - 1, i = editbuf->n_prevs - 1;
			line >= 0; line--, i--)
		mvwprintw (w->win, line, 0, "C%s", i >= 0 ? prevs[i] : "");
}

widget make_cmdline_window_widget (tui *) {
	static displayed_elem_normal_mode_fns displayed_elems_fns = {
		.get_idx_of_first_element = get_idx_of_first_element,
		.get_element_count        = get_element_count,
		.elem_tl_from_cursor      = elem_tl_from_cursor,
		.elem_br_from_cursor      = elem_br_from_cursor,
		.cursor_y_from_pad_line   = cursor_y_from_pad_line,
		.elem_from_cursor         = 0,
	};
	return (widget) {
		.name = "cmdline-window",
		.description = "Edit previous commands, run them",
		.fns = &displayed_elems_fns,
		.required_screen_wh = (vec) {.y = 7},
		.modes = make_op_modes (),
		.draw = draw,
	};
}
