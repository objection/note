#include "../generic-ops.h"

static MAKE_OP_FUNC (cancel) {
	cur_editbuf (t)->n = 0;
	t->break_out = true;
	return 0;
}

static MAKE_OP_FUNC (accept) {
	t->break_out = true;
	return 0;
}

static widget_op_modes make_op_modes (void) {
	struct widget_op_mode widget_op_mode = {
		.default_op = make_default_op (default_op_self_insert, "Insert character")
	};

	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Cancel",           op_cancel,             TUI_CTRL ('c'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Cancel",           op_cancel,             TUI_CTRL (' '));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Accept",           op_accept,             TUI_CTRL ('m'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Accept",           op_accept,             TUI_CTRL ('\r'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete char",      op_delete,             KEY_BACKSPACE);
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete char",      op_delete,             0x7f);
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete char",      op_delete,             TUI_CTRL ('h'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Kill line",        op_delete_to_start,    TUI_CTRL ('u'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete prev word", op_delete_word_behind, TUI_CTRL ('w'));

	// This is a hack to make shift-space not ultimately close the
	// cmdline when running in GVIM. The reason it does that is
	// because GVIM's terminal sets the modifyOtherKeys thing, which
	// makes shift-space send [[32;2u.
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "A hack, ignore",    0,   TUI_CTRL ('s'));

	return make_widget_op_modes (1, &widget_op_mode);
}

static vec elem_tl_from_cursor (tui *, vec) {
	return (vec) {0, 0};
}

static void draw (struct tui *t, widget *w, int) {
	draw_cmdlinelike_widget (t, w, "I");
}

widget make_input_widget (tui *) {
	static displayed_elem_normal_mode_fns displayed_fns = {
		.elem_tl_from_cursor = elem_tl_from_cursor,
	};
	return (widget) {
		.name = "input",
		.description = "Input something",
		.required_screen_wh = (vec) {.y = 1},
		.modes = make_op_modes (),
		.fns = &displayed_fns,
		.draw = draw,
	};
}
