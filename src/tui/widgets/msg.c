#include "msg.h"
#include "statusline.h"
#include "../generic-ops.h"

enum {
	MSG_MODE_SHORT,
	MSG_MODE_LONG,
	N_MSG_MODES ,
};

// All this stuff could be put in a "lines-widget-fns.c" file becaus
// they're the same ones that are used in the cmdline window. But I
// don't know if they're remain the same. So let's noet.
static int get_idx_of_first_element (tui *, widget *) {
	return 0;
}

static int get_element_count (tui *, widget *w) {
	return w->required_screen_wh.y;
}

static vec elem_tl_from_cursor (struct tui *, struct vec cursor) {
	return cursor;
}

static vec elem_br_from_cursor (struct tui *, struct vec cursor) {
	return cursor;
}

static struct vec cursor_y_from_pad_line (struct tui *, int line) {
	return (struct vec) {.y = line};
}

static void draw (struct tui *, widget *, int) {
}

static widget_op_mode make_long_msg_widget_op_mode () {
	struct op_arr ops = {};
	push_generic_screenmoving_ops (&ops);
	PUSH_NO_ARGS_OP (&ops, "Toggle this",       op_toggle_keys,         'K');
	PUSH_NO_ARGS_OP (&ops, "Scroll down",       op_c_e,                 'j');
	PUSH_NO_ARGS_OP (&ops, "Scroll up",         op_c_y,                 'k');
	PUSH_NO_ARGS_OP (&ops, "Scroll down",       op_c_e,                 'e');
	PUSH_NO_ARGS_OP (&ops, "Scroll up",         op_c_y,                 'y');
	PUSH_NO_ARGS_OP (&ops, "Down half page",    op_c_d,                 'd');
	PUSH_NO_ARGS_OP (&ops, "Up half page",      op_c_u,                 'u');
	PUSH_NO_ARGS_OP (&ops, "Down 1 page",       op_c_f,                 'f');
	PUSH_NO_ARGS_OP (&ops, "Up 1 page",         op_c_b,                 'b');
	PUSH_NO_ARGS_OP (&ops, "Go to bottom",      op_G,                   'G');
	PUSH_NO_ARGS_OP (&ops, "Go to top",         op_gg,                  'g', 'g');
	PUSH_NO_ARGS_OP (&ops, "Leave msg",         op_go_to_parent_widget, 'q');
	return (widget_op_mode) {.ops = ops};
}

static MAKE_DEFAULT_OP (exit_short_msg) {
	t->widgets->current = w->parent_widget;
	ungetch (ch);
	/* input (t); */
}

static widget_op_mode make_short_msg_widget_op_mode () {
	return (widget_op_mode) {
		.default_op = make_default_op (default_op_exit_short_msg,
				"Dismiss message, passing key along"),
	};
}

static widget_op_modes make_modes () {
	return make_widget_op_modes (N_MSG_MODES, (widget_op_mode [N_MSG_MODES]) {
			[MSG_MODE_LONG]  = make_long_msg_widget_op_mode (),
			[MSG_MODE_SHORT] = make_short_msg_widget_op_mode (),
		});
}

static void draw_statusline (struct tui *, widget *w) {
	mvwaddstr (w->win, 0, 0, "Message");
}

static widget_op_mode *get_op_mode (widget *w) {
	if (w->required_screen_wh.y < LINES - 1 - 1)
		return w->modes.d + MSG_MODE_SHORT;
	return w->modes.d + MSG_MODE_LONG;
}

widget make_msg_widget (tui *) {
	static displayed_elem_normal_mode_fns fns = {
		.get_idx_of_first_element = get_idx_of_first_element,
		.get_element_count        = get_element_count,
		.cursor_y_from_pad_line   = cursor_y_from_pad_line,
		.elem_tl_from_cursor      = elem_tl_from_cursor,
		.elem_br_from_cursor      = elem_br_from_cursor,
	};

	widget *statusline_widget = malloc (sizeof *statusline_widget);
	*statusline_widget = make_statusline_widget (nullptr, "msg statusline",
			draw_statusline);

	return (widget) {
		.name = "msg",
		.description = "A message from me to you",
		.draw = draw,
		.modes = make_modes (),
		.fns = &fns,
		.statusline_widget = statusline_widget,
		.get_op_mode = get_op_mode,
	};
}
