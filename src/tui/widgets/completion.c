#include "completion.h"
#include <darr.h>

enum {
	MAX_COMPL_FMT_STR = 64,

	// Potentially this could be just "POPUP_LEFT_MARGIN". I suppose
	// you could have both, coudn't you.
	COMPLETION_LEFT_MARGIN = 1,
	COMPLETION_RIGHT_MARGIN = 1,
};

static MAKE_OP_FUNC (completions_increment_cursor) {
	increment_cursor (t, w);
	insert_word_into_editbuf (cur_editbuf (t), t->completions.completion_word_start,
			t->completions.matches[cur_widget (t)->cursor.y].str,
			w->parent_widget);
	return 0;
}

static MAKE_OP_FUNC (completions_decrement_cursor) {
	decrement_cursor (t, w);
	insert_word_into_editbuf (cur_editbuf (t), t->completions.completion_word_start,
			t->completions.matches[cur_widget (t)->cursor.y].str,
			w->parent_widget);
	return 0;
}

static int get_idx_of_first_element (tui *, widget *) {
	return 0;
}

static int get_element_count (tui *t, widget *) {
	return t->completions.count;
}

static vec elem_tl_from_cursor (tui *, vec cursor) {
	return cursor;
}

static vec elem_br_from_cursor (tui *, vec cursor) {
	return cursor;
}

static struct vec cursor_y_from_pad_line (struct tui *, int line) {
	return (struct vec) {.y = line};
}

static MAKE_OP_FUNC (accept) {
	cur_editbuf (t)->n = t->completions.completion_word_start;
	char *word = t->completions.matches[cur_widget (t)->cursor.y].str;
	int word_len = strlen (word);
	arr_insert_mul (cur_editbuf (t), t->completions.completion_word_start, word, word_len);
	t->widgets->current = &t->widgets->as.cmdline;
	cur_widget (t)->cursor.x = t->completions.completion_word_start + word_len;
	nuke_completions (&t->completions);
	return 0;
}

static MAKE_OP_FUNC (discard) {
	auto cmdline_widget_cursor = &t->widgets->as.cmdline.cursor;
	arr_splice (cur_editbuf (t), t->completions.cursor_pos_before_completion_start,
			cmdline_widget_cursor->x - t->completions.cursor_pos_before_completion_start);

	cmdline_widget_cursor->x = t->completions.cursor_pos_before_completion_start;
	t->widgets->current = w->parent_widget;
	nuke_completions (&t->completions);
	return 0;
}

static MAKE_DEFAULT_OP (exit_widget) {

	nuke_completions (&t->completions);
	go_to_a_cmdline (t, w->parent_widget, EID_CMD);
	ungetch (ch);
}

static widget_op_modes make_op_modes (void) {
	widget_op_mode widget_op_mode = {
		.default_op = make_default_op (default_op_exit_widget,
				"Stop completion and insert character"),
	};

	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Accept",     op_accept,                       TUI_CTRL ('y'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Discard",    op_discard,                      TUI_CTRL ('e'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Next match", op_completions_increment_cursor, TUI_CTRL ('n'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Next match", op_completions_increment_cursor, '\t');
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Prev match", op_completions_decrement_cursor, TUI_CTRL ('p'));

	return make_widget_op_modes (1, &widget_op_mode);
}

static void draw_simple_str_completion (tui *t, int pad_y,
		char fmt_str[static MAX_COMPL_FMT_STR]) {

	auto w = &t->widgets->as.completion;

	auto match = &t->completions.matches[pad_y];

	wprintw (w->win, fmt_str, match->str);
}

static void draw_keyword_completion (tui *t, int pad_y, char *fmt_str) {

	auto w = &t->widgets->as.completion;

	char match_and_arg_buf[256];

	char *p = match_and_arg_buf;

	auto match = &t->completions.matches[pad_y];

	p += snprintf (match_and_arg_buf, sizeof match_and_arg_buf, "%s", match->str);

	auto keyword = &match->keyword->keyword;
	auto args_descr = t->completions.matches[pad_y].keyword->keyword.args_descr;

	if (args_descr) {
		if (keyword->args_are_optional) {
			p += snprintf (p, sizeof match_and_arg_buf, "[==%s]",
					match->keyword->keyword.args_descr);
		} else {
			p += snprintf (p, sizeof match_and_arg_buf, "[=%s]",
					match->keyword->keyword.args_descr);
		}
	}

	// We really need some kind of static string builder. The
	// truth is checking we've gone over is annoying, so I know
	// there's a lot of places I'm not doing it.
	ASSERT (p - match_and_arg_buf < (signed) sizeof match_and_arg_buf);

	wprintw (w->win, fmt_str, match_and_arg_buf,
			match->keyword->keyword.short_help_descr);
}

static void draw (struct tui *t, widget *w, int) {

	wbkgdset (w->win, A_REVERSE);

	werase (w->win);

	// We wmove in a bit.

	maybe_resize_pad_to_fit_new_lines (w->win, t->completions.count);

	char fmt_str_buf[64];

	switch (t->completions.type) {

	case COMPLETION_TYPE_TSK:
		snprintf (fmt_str_buf, sizeof fmt_str_buf, "%%-%ds %%s",
				t->completions.max_match_and_arg_len);
		break;
	case COMPLETION_TYPE_SIMPLE_STR:
		snprintf (fmt_str_buf, sizeof fmt_str_buf, "%%-%ds",
				t->completions.max_match_and_arg_len);

		break;
	}

	for (int pad_y = 0; pad_y < t->completions.count; pad_y++) {

		wmove (w->win, pad_y, 0);

		for (int i = 0; i < COMPLETION_LEFT_MARGIN; i++)
			waddch (w->win, ' ');

		switch (t->completions.type) {
		case COMPLETION_TYPE_TSK:
			if (!t->completions.is_arg)
				draw_keyword_completion (t, pad_y, fmt_str_buf);
			else
				draw_simple_str_completion (t, pad_y, fmt_str_buf);
			break;
		case COMPLETION_TYPE_SIMPLE_STR:
			draw_simple_str_completion (t, pad_y, fmt_str_buf);
			break;
		}

		for (int i = 0; i < COMPLETION_RIGHT_MARGIN; i++)
			waddch (w->win, ' ');
	}
}

int get_compl_required_screen_width (int n_str_lens, int str_lens[n_str_lens]) {
	int r = COMPLETION_LEFT_MARGIN;

	for (int i = 0; i < n_str_lens; i++)
		r += str_lens[i];

	// The spaces between the words.
	r += n_str_lens - 1;

	r += COMPLETION_RIGHT_MARGIN;
	return r;
}

widget make_completion_widget (tui *) {
	static displayed_elem_normal_mode_fns displayed_elems_fns = {
		.get_idx_of_first_element = get_idx_of_first_element,
		.get_element_count        = get_element_count,
		.elem_tl_from_cursor      = elem_tl_from_cursor,
		.elem_br_from_cursor      = elem_br_from_cursor,
		.cursor_y_from_pad_line   = cursor_y_from_pad_line,
		.elem_from_cursor         = nullptr,
	};

	return (widget) {
		.name = "completions",
		.description = "Replace word with chosen match",
		.is_popup = true,
		.fns = &displayed_elems_fns,
		.modes = make_op_modes (),
		.draw = draw,
	};
}
