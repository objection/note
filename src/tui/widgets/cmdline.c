#define _GNU_SOURCE

#include "cmdline.h"
#include "../generic-ops.h"
#include "../widgets/completion.h"
#include "../cmd.h"
#include <rab.h>
#include <ctype.h>

static MAKE_OP_FUNC (run_cmd) {

	int r = op_change_mode_to_normal (op_args, t, w);
	if (t->editbufs.id_current == EID_CMD)

		// Never mind the error code. Hopefully there was an error
		// message.
		tui_split_cmdline_and_do_cmd (t);

	// FIXME: I'm using the second editbuf for the input callback.
	// 2024-06-18T21:25:02: I'm not sure if this does anything now.
	else if (t->editbufs.id_current == EID_CONFIRM)
		arr_add (cur_editbuf (t), 0);
	else
		tui_search (t, op_args[0].b);
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return r;
}

static void set_keyword_arg_completion_text (tui *t) {
	int max_match_len = 0;

	EACH (match, t->completions.count, t->completions.matches) {
		int match_len = strlen (match->str);

		UPDATE_MAX (&max_match_len, match_len);
	}

	t->completions.max_match_and_arg_len = max_match_len;
	t->widgets->as.completion.required_screen_wh.x = t->completions.max_match_and_arg_len +
		2;
}

static void draw (struct tui *t, widget *w, int) {
	draw_cmdlinelike_widget (t, w, t->editbufs.id_current ? "S" : "C");
}

static void set_keyword_completion_text (tui *t) {
	int max_match_len = 0;
	int max_arg_len = 0;
	int max_help_descr = 0;

	// This code or the code in draw_completion_window has off by
	// a column or two. But it produces the right amount of
	// spacing between the match str and arg str and description
	// so I'm not going to worry about. I should, but I'll move
	// on.

	EACH (match, t->completions.count, t->completions.matches) {
		auto keyword = &match->keyword->keyword;
		int match_len = strlen (match->str);

		UPDATE_MAX (&max_match_len, match_len);

		// This is the arg str, eg "PATH", in the form, eg
		// "[=PATH]". For optional args it's "[==PATH]".
		if (keyword->args_descr) {
			int arg_len =

				// "[=]"
				3 +
				strlen (keyword->args_descr);

			if (keyword->args_are_optional)

				// We indicate optional args with a second "="
				arg_len += 1;
			UPDATE_MAX (&max_arg_len, arg_len);
		}
		if (match->keyword->keyword.short_help_descr)
			UPDATE_MAX (&max_help_descr,
					(int) strlen (match->keyword->keyword.short_help_descr));
	}

	t->completions.max_match_and_arg_len = max_match_len + max_arg_len;

	t->widgets->as.completion.required_screen_wh.x = get_compl_required_screen_width (2,
			(int []) {t->completions.max_match_and_arg_len, max_help_descr});
}

static void set_str_completion_text (tui *t) {
	int max_match_len = 0;
	EACH (match, t->completions.count, t->completions.matches) {
		int match_len = strlen (match->str);
		UPDATE_MAX (&max_match_len, match_len);
	}
	t->completions.max_match_and_arg_len = max_match_len;
	t->widgets->as.completion.required_screen_wh.x = get_compl_required_screen_width (1,
			(int []) {t->completions.max_match_and_arg_len});
}

static void set_tsk_completion_text (tui *t) {
	if (!t->completions.is_arg)
		set_keyword_completion_text (t);
	else
		set_keyword_arg_completion_text (t);
}

static int start_completion (tui *t, widget *w) {
	auto editbuf = cur_editbuf (t);

	t->completions.cursor_pos_before_completion_start = w->cursor.x;
	t->completions.completion_word_start = ({
		int it = w->cursor.x;
		while (it > 0 && !isspace (editbuf->d[it]))
			it--;
		if (it)
			it++;
		it;
	});

	t->completions.max_match_and_arg_len = 0;

	ASSERT (t->completions.count);

	if (t->completions.count == 1) {
		insert_word_into_editbuf (editbuf, t->completions.completion_word_start,
				t->completions.matches[0].str, cur_widget (t));
		return 0;
	}

	t->widgets->as.completion.required_screen_wh.y = t->completions.count;

	switch (t->completions.type) {
	case COMPLETION_TYPE_TSK:        set_tsk_completion_text (t); break;
	case COMPLETION_TYPE_SIMPLE_STR: set_str_completion_text (t); break;
	}

	insert_word_into_editbuf (editbuf, t->completions.completion_word_start,
			t->completions.matches[0].str, cur_widget (t));

	t->widgets->current = &t->widgets->as.completion;

	cur_widget (t)->cursor = (vec) {};
	cur_widget (t)->parent_widget = &t->widgets->as.cmdline;
	cur_widget (t)->specified_tl = (vec) {

		// HARD-CODED.
		.y = LINES - 1 - t->completions.count,
			.x =

				// There's always a "C" at the start of the cmdline.
				1 +
				t->completions.completion_word_start - 1,
	};

	t->flags |= TF_RECREATE_WINDOW_CONTENTS;

	return 0;
}

static MAKE_OP_FUNC (start_tsk_completion) {

	auto editbuf = cur_editbuf (t);

	// If there's one byte allocated and so cursor->n, you don't want
	// to set editbuf->d[cursor->n] to 1 without pushing something
	// first.
	tsk_completions tsk_completions = {};
	char saved_char = editbuf->d[editbuf->n];
	editbuf->d[w->cursor.x] = '\0';
	tsk_get_completions_from_str (&t->tsk_ctx, editbuf->d, cur_cmd (t),
			&tsk_completions);
	editbuf->d[w->cursor.x] = saved_char;

	if (tsk_completions.count == 0)
		return 0;

	// We turn the tsk completions into a format that can be used by
	// both tsk completions and TUI completions. That format is
	// basically identical to the tsk one.
	for (int i = 0; i < tsk_completions.count; i++) {
		t->completions.matches[i] = (completion_match) {

			// We don't strdup, and we don't free it here. We free it
			// in nuke_completions, for better or worse.
			.str = tsk_completions.matches[i].str,
			.keyword = tsk_completions.matches[i].keyword,
		};
	};
	t->completions.is_arg = tsk_completions.is_arg;
	t->completions.count = tsk_completions.count;

	return start_completion (t, w);
}

static MAKE_OP_FUNC (backward_search_history) {

	// Right now there's no op_forward_search_history, reason being
	// I've never ever searched forwards through the history in Zsh,
	// or Vim. I think the reason is it's difficult; there's no
	// indicator in either program of which history line you're on. So
	// I'm just not going to put the feature in. Maybe I'll regret
	// that later when I decide I need it and have to figure out how I
	// did it this bit.

	auto editbuf = cur_editbuf (t);

	if (!editbuf->n_prevs)
		return 0;

	// We don't skip initial whitespace in the editbuf. It seems to me
	// that would be a sensible thing to do, but Vim doesn't do it,
	// and, thinking about it, I've never had some sort of problem
	// with initial whitespace. So let's leave it, let's keep things
	// simple for me and you (me).

	if (editbuf->n == 0) {
		for (int i = 0; i < editbuf->n_prevs; i++)
			t->completions.matches[i].str = strdup (editbuf->prevs[i]);
		t->completions.count = editbuf->n_prevs;
	} else {
		int __idxes[editbuf->n];
		rab_completion_matches matches = {.idxes = __idxes};

		char saved_char = editbuf->d[editbuf->n];
		editbuf->d[w->cursor.x] = '\0';
		rab_get_completion (editbuf->d,
						editbuf->n_prevs, editbuf->prevs, nullptr, editbuf->n_prevs,
						&matches);
		editbuf->d[w->cursor.x] = saved_char;

		for (int i = 0; i < matches.count; i++)
			t->completions.matches[i].str = strdup (editbuf->prevs[matches.idxes[i]]);

		t->completions.count = matches.count;
	}
	w->cursor.y = t->completions.count;
	t->completions.type = COMPLETION_TYPE_SIMPLE_STR;

	return start_completion (t, w);
}

static vec elem_tl_from_cursor (tui *, vec) {
	return (vec) {0, 0};
}

static MAKE_OP_FUNC (add_item_from_current_cmdline) {

	auto editbuf = cur_editbuf (t);

	// We just paste ":edit" on to the end of the command. This means
	// if you haven't closed your quotes you'll get surprising
	// results. I could insert the quote for you, and maybe I will
	// later. You'll want to use a stack.

	arr_add (editbuf, ' ');
	arr_add (editbuf, ':');
	arr_add (editbuf, 'e');
	arr_add (editbuf, 'd');
	arr_add (editbuf, 'i');
	arr_add (editbuf, 't');

	endwin ();

	tui_split_cmdline_and_do_cmd (t);
	doupdate ();
	t->widgets->current = &t->widgets->as.items;
	return 0;
}

static widget_op_modes make_op_modes (void) {
	struct widget_op_mode widget_op_mode = {
		.default_op = make_default_op (default_op_self_insert, "Insert character")
	};

	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Toggle this",       op_toggle_keys,                   'K');
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Edit new item",     op_add_item_from_current_cmdline, TUI_CTRL ('g'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Start completion",  op_start_tsk_completion,          '\t');
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Exit cmdline",      op_change_mode_to_normal,         TUI_CTRL ('c'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Exit cmdline",      op_change_mode_to_normal,         TUI_CTRL (' '));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Exit cmdline",      op_change_mode_to_normal,         TUI_CTRL ('['));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Run cmd",           op_run_cmd,                       TUI_CTRL ('m'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Run cmd",           op_run_cmd,                       TUI_CTRL ('\r'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete char",       op_delete,                        KEY_BACKSPACE);
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete char",       op_delete,                        0x7f);
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete char",       op_delete,                        TUI_CTRL ('h'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Kill line",         op_delete_to_start,               TUI_CTRL ('u'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Delete prev word",  op_delete_word_behind,            TUI_CTRL ('w'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Strt cmdline win",  op_change_mode_to_cmdline_win,    TUI_CTRL ('f'));
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "Complete history",  op_backward_search_history,       TUI_CTRL ('p'));
	// There's no "op_forward_search_history", for reasons explained
	// up there.

	// This is a hack to make shift-space not ultimately close the
	// cmdline when running in GVIM. The reason it does that is
	// because GVIM's terminal sets the modifyOtherKeys thing, which
	// makes shift-space send [[32;2u.
	PUSH_NO_ARGS_OP (&widget_op_mode.ops, "A hack, ignore",    0,    TUI_CTRL ('s'));

	return make_widget_op_modes (1, &widget_op_mode);
}

widget make_cmdline_widget (tui *) {
	static displayed_elem_normal_mode_fns displayed_fns = {
		.elem_tl_from_cursor = elem_tl_from_cursor,
	};
	return (widget) {
		.name = "cmdline",
		.description = "Type a Note cmd to filter and act on items",
		.required_screen_wh = (vec) {.y = 1},
		.modes = make_op_modes (),
		.fns = &displayed_fns,
		.draw = draw,
	};
}
