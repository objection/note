#pragma once
#include <libnote/libnote.h>
#include <ncurses.h>
#include "../cli/args.h"

enum {
	MAX_COMPLETIONS = 256,
	MAX_HISTORY = 1024,
};

// Cursor are used for all widgets, even though only the cmdline
// window has any need for "col".
typedef struct vec vec;
struct vec {
	int y, x;
};

typedef struct rect rect;
struct rect {
	vec tl,

		// Inclusive.
		br;
};

typedef struct tui_view_elem tui_view_elem;
struct tui_view_elem {
	struct rect rect;

		// Used for items; probably ignored for confirmations.
	int item_idx;
};

typedef struct tui_view tui_view;
struct tui_view {
	tui_view_elem *d;
	ssize_t n, a;
};

typedef struct tsk_cmd_arr tsk_cmd_arr_s;
struct tsk_cmd_arr {
	tsk_cmd *d;
	ssize_t n, a;
	int idx;
};

#define TUI_CTRL(c) ((c) & 037)

enum editbuf_id {
	EID_CMD,
	EID_SEARCH,
	EID_CONFIRM,
	EID_N,
};

typedef struct editbuf editbuf;
struct editbuf {
	char *d;
	size_t n, a;
	bool forwards;
	char *prevs[MAX_HISTORY];
	int n_prevs;
};

$make_static_arr_with_member (struct editbuf, editbufs_static_nctk, EID_N,
		enum editbuf_id id_current);

enum tui_flags {

	// This is named what it is to hopefully remind me that when it's
	// set we redraw or repaint everything, that is we call tsk_print
	// or mvwprintw or whatever. It doesn't mean that we don't
	// pnoutrefresh and doupdate the screen until this happens.
	TF_RECREATE_WINDOW_CONTENTS = 1 << 0,
	TF_HAS_LAST_SEARCH = 1 << 1,
};

enum completions_type {
	COMPLETION_TYPE_TSK,
	COMPLETION_TYPE_SIMPLE_STR,
};

typedef struct completion_match completion_match;

// This is conceptually a union. The tsk_completion_match gets copied
// into here, and it has the same members. The other kind of
// completion goes into str only (for now). We can't really use a
// union. Trust me.
struct completion_match {
	char *str;
	tsk_cmd_keyword *keyword;
};

typedef struct completions completions;
struct completions {
	completion_match matches[MAX_COMPLETIONS];
	int count;
	bool is_arg;

	enum completions_type type;

	int max_match_and_arg_len;
	int completion_word_start;
	int cursor_pos_before_completion_start;
};

enum keys_justification {
	KJ_LEFT,
	KJ_RIGHT,
};

typedef struct tui tui;
struct tui {

	bool break_out;

	// This is just a quick hack. A better way might be to have a
	// "show" member in each widget struct.
	bool show_keys_widget;
	struct partiallymatched_ops partiallymatched_ops;
	enum keys_justification keys_justification;

	tsk_ctx tsk_ctx;

	enum colour_mode colour_mode;
	int lines_to_print;
	int userspecified_cols;

	// Need this so I can print out the whole note if j and you're on
	// the last note.
	int repeat;

	// This is a flat array. Libnote deals with statements and cmds,
	// even if it doesn't look like that since the CLI prints all of
	// them. But the TUI works with a flat array of items. It's just
	// easier. Well, I presume so. I'm about to change it so it's like
	// that just now.
	struct tui_view tui_item_view;

	// We *absolutely* need something like this. As yourself how the
	// colour callbacks are meant to know which window to draw to
	// without saving this.
	struct widget *widget_we_are_printing_to;

	// NOTE: maybe this should go in no.
	int n_cmd_history;

	// These have to be here they need to be in print_ctx, draw and
	// tui_confirm.
	struct tsk_cmd_arr cmds;
	struct tsk_confirmations *confirmations;
	completions completions;

	struct editbufs_static_nctk editbufs;

	// Needs something like this to push item_dims.
	int statement_i;

	struct widgets *widgets;
	struct widget *print_to_widget;
	enum tui_flags flags;
};

static inline struct editbuf *cur_editbuf (tui *t) {
	return &t->editbufs.d[t->editbufs.id_current];
}

static inline struct tsk_cmd *cur_cmd (tui *t) {
	return &t->cmds.d[t->cmds.idx];
}

static inline struct tsk_statement *cur_statement(tui *t) {
	return &(cur_cmd (t))->statements.d[t->statement_i];
}

void run_tui (tsk_ctx *tsk_ctx, tsk_cmd *cmd, tsk_options *to,
		enum colour_mode colour_mode, int lines_to_print, int userspecified_cols);
[[gnu::format (printf, 4, 5)]]
int mv_print_fmt (WINDOW *win, int y, int x, char *fmt, ...);
[[gnu::format (printf, 2, 3)]]
int print_fmt (WINDOW *win, char *fmt, ...);
void maybe_resize_pad_to_fit_new_lines (WINDOW *win, int n_lines);
void nuke_completions (completions *completions);
int tui_tsk_print (struct tsk_print_ctx *pc, char *fmt, ...);
