#define _GNU_SOURCE

#include "tui.h"
#include "widget.h"
#include "draw.h"
#include "input.h"
#include "misc.h"
#include "cmd.h"
#include <locale.h>
#include <rab.h>

short TSK_COLOUR_PAIRS[TSK_N_COLOUR] = {
	[TSK_COLOUR_OFF] = -1,  // ignore
	[TSK_COLOUR_PRINT_HEADER] = COLOR_CYAN,
	[TSK_COLOUR_CHANGE_HEADER] = COLOR_YELLOW,
	[TSK_COLOUR_NO_MATCH_HEADER] = COLOR_MAGENTA,
	[TSK_COLOUR_ERROR_HEADER] = COLOR_RED,
	[TSK_COLOUR_ERROR_INDICATORS] = COLOR_RED,
};

static void init_ncurses () {
	setlocale (LC_ALL, "");
	initscr ();
	raw ();
	noecho ();
	nonl ();
	scrollok (stdscr, 1);
	intrflush (stdscr, FALSE);
	keypad (stdscr, TRUE);
	start_color ();

	assume_default_colors (-1,-1);
#if 0
	win_status = newwin (cmdline_n_lines, 0,
			LINES - cmdline_n_lines - status_n_lines, 0);
#endif
	for (enum tsk_colour id = 0; id < TSK_N_COLOUR; id++)
		init_pair (id, TSK_COLOUR_PAIRS[id], -1);
}

static void close_ncurses () {
	endwin ();
}

enum window_type {
	WINDOW_TYPE_NOTES,
	WINDOW_TYPE_LINE,
};

static int update () {
	return 0;
}

void nuke_completions (completions *completions) {
	for (int i = 0; i < completions->count; i++)
		free (completions->matches[i].str);
	*completions = (struct completions) {};
}

static int tui_colour (struct tsk_print_ctx *pc) {
	struct tui *t = pc->user;

	// This is dodgy. It's clearly going to make things strange
	// when we change widgets.
	static int last_colour = -1;

	auto w = t->widget_we_are_printing_to;

	if (pc->colour_index == TSK_COLOUR_OFF)
		wattroff (w->win, COLOR_PAIR (last_colour));
	else {
		wattr_set (w->win, A_NORMAL, pc->colour_index, nullptr);
		last_colour = pc->colour_index;
	}
	return 0;
}

static int get_win_height (WINDOW *win) {

	// There's proper ncurses functions you can use for this but the
	// documentation doesn't say much about them and I can't be
	// bothered to test shit out. The height of the window as far as
	// this program is concerned should be the current y + 1 because
	// the current y should always be the latests position. We just
	// draw from top to bottom.

	return getcury (win) + 1;
}

void maybe_resize_pad_to_fit_new_lines (WINDOW *win, int n_lines) {

	// Resize the pad if necessary. We don't resize the cols here. The
	// cols are set to be COLS and resized in the resize function.

	int pad_h, pad_w;
	getmaxyx (win, pad_h, pad_w);

	int height = get_win_height (win);
	if (height + n_lines >= pad_h)
		wresize (win, pad_h + n_lines * 2, pad_w);
}

static int print_fmt_ap (WINDOW *win, char *fmt, va_list ap) {
   	auto buf = rab_get_str_ap (fmt, ap);
	ASSERT (buf);

	// ncurses routines don't return the length of the str.
	int r = strlen (buf);

	int n_lines = count_occurences_of_chars (buf, '\n');
	char *end = strchr (buf, 0);
	if (end[-1] != '\n')
		n_lines++;

	maybe_resize_pad_to_fit_new_lines (win, n_lines);

	waddstr (win, buf);
	free (buf);
	return r;
}

int mv_print_fmt (WINDOW *win, int y, int x, char *fmt, ...) {

	wmove (win, y, x);

	// Use this instead of waddstr eg because it will resize the pad.
	va_list ap;
	va_start (ap);
	int r = print_fmt_ap (win, fmt, ap);
	va_end (ap);
	return r;
}

int print_fmt (WINDOW *win, char *fmt, ...) {

	// Use this instead of waddstr eg because it will resize the pad.
	va_list ap;
	va_start (ap);
	int r = print_fmt_ap (win, fmt, ap);
	va_end (ap);
	return r;
}

int tui_tsk_print (struct tsk_print_ctx *pc, char *fmt, ...) {
	struct tui *t = pc->user;
	auto w = t->widget_we_are_printing_to;

	va_list ap;
	va_start (ap);

	int r = print_fmt_ap (w->win, fmt, ap);
	va_end (ap);
	return r;
}

static int tui_tsk_msg_ap (struct tsk_print_ctx *pc, char *fmt, va_list ap) {
	tui *t = pc->user;
	widget *msg_w = &t->widgets->as.msg;

	// This, btw, is where everything to do with drawing messages
	// takes place. It has to be here. Except that really I should get
	// rid of the callback functions and then we wouldn't have to do
	// this. We'd return a struct an expose a print message.
	switch (pc->progress) {
	case TMPS_BEGIN:
		msg_w->required_screen_wh = msg_w->pad_tl = msg_w->cursor = (vec) {};
		werase (msg_w->win);
		wmove (msg_w->win, 0, 0);
		return 0;
	case TMPS_DONE:
		msg_w->required_screen_wh.y = get_win_height (msg_w->win);

		// Here is where we set the widget to the msg widget. You have
		// to do it here.
		msg_w->parent_widget = t->widgets->current;
		t->widgets->current = msg_w;
		return 0;
	default:
		return print_fmt_ap (msg_w->win, fmt, ap);
	}
}

static int tui_tsk_msg (struct tsk_print_ctx *pc, char *fmt, ...) {
	va_list ap;
	va_start (ap);

	int r = tui_tsk_msg_ap (pc, fmt, ap);

	va_end (ap);
	return r;
}

static struct tui *set_up_tui (struct tsk_cmd *cmd,
		enum colour_mode colour_mode, int lines_to_print, int userspecified_cols) {
	cmd->source = CMD_SOURCE_FRONTEND;

	cmd->user = CALLOC_1 (sizeof (struct vec));
	*(struct vec *) cmd->user = (struct vec) {};

	// Need to allocate because of pc.user. I don't see why. The
	// problem is the pointer to w, but so what?
	struct tui *r = CALLOC_1 (sizeof *r);

	r->show_keys_widget = true;
	r->colour_mode = colour_mode;
	r->lines_to_print = lines_to_print;
	r->userspecified_cols = userspecified_cols;
	r->n_cmd_history = 100;

	arr_add (&r->cmds, *cmd);
	r->flags |= TF_RECREATE_WINDOW_CONTENTS;
	r->widgets = set_up_widgets (r);

	// Need-to-set-the-widget-we-are-printing-to to the msg one in
	// case a msg happens first. The reason we need to store
	// something like this is we don't have control over when messages
	// are sent and we want to have just one print-colour function.
	r->widget_we_are_printing_to = &r->widgets->as.msg;

	// NOTE: these should really be allocated when they're used.
	// I'm not happy about all of this allocating. But the fact is
	// I'm going to have cmd histories, just like Vim does.
	FORI (i, countof (r->editbufs.d))
		arr_init (&r->editbufs.d[i]);

	arr_insert_mul (&r->editbufs.d[0], 0, cmd->text, (int) strlen (cmd->text));
	create_notefile_stats ();
	return r;
}

#if 0
char *tui_get_input (struct tsk_print_ctx *p, char *fmt, ...) {
	va_list ap;
	va_start (ap);
	va_end (ap);
}
#endif

static bool tui_confirm (struct tsk_print_ctx *pc, char *fmt, ...) {
	va_list ap;
	va_start (ap);
	tui_tsk_msg_ap (pc, fmt, ap);
	va_end (ap);

	tui *t = pc->user;

	// Need to draw because we need to return your result to libnote.
	// You need to see the msg so you can say yn.
	draw (t);
	int ch = wgetch (t->widgets->as.msg.win);
	bool r = ch == 'y' ? 1: 0;

	t->flags |= TF_RECREATE_WINDOW_CONTENTS;

	return r;
}

static int input_update_draw_loop (tsk_ctx *tsk_ctx) {
	struct tui *t = tsk_ctx->pc->user;
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	draw (t);
	for (;;) {
		t->flags |= TF_RECREATE_WINDOW_CONTENTS;
		if (input (t))
			return 1;
		if (t->break_out) {
			t->break_out = false;
			break;
		}
		draw (t);
	}
	return 0;
}

static int tui_multi_confirm (tsk_ctx *tsk_ctx, tsk_cmd *,
		struct tsk_confirmations *confirmations) {
	struct tui *t = tsk_ctx->pc->user;
	t->confirmations = confirmations;
	t->widgets->current = &t->widgets->as.multi_confirm;
	input_update_draw_loop (tsk_ctx);
	t->widgets->current = &t->widgets->as.items;
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return 0;
}

static int tui_input (int n_out, char *out, tsk_print_ctx *pc, char *fmt, ...) {
	va_list ap;
	va_start (ap);

	struct tui *t = pc->user;

	// Reusing the cmdline's editbuf. This is lazy.
	cur_editbuf (t)->n = t->widgets->as.input.cursor.x = 0;

	t->widgets->current = &t->widgets->as.input;

	ASSERT (cur_widget (t) == &t->widgets->as.input);

	print_fmt_ap (t->widgets->as.msg.win, fmt, ap);
	t->widgets->as.msg.required_screen_wh.y = get_win_height (t->widgets->as.msg.win);

	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	draw (t);

	va_end (ap);

	input_update_draw_loop (&t->tsk_ctx);

	int r = 1;
	if (cur_editbuf (t)->n) {
		arr_add (cur_editbuf (t), 0);

		// Copying more than we need. Also not considering sentinel --
		// BUG.
		memcpy (out, cur_editbuf (t)->d, n_out * sizeof *cur_editbuf (t)->d);
		arr_pop (cur_editbuf (t));
		push_editbuf (cur_editbuf (t));
		r = 0;
	}

	t->widgets->current = &t->widgets->as.items;

	return r;
}

[[noreturn]]
void run_tui (tsk_ctx *tsk_ctx, tsk_cmd *cmd, tsk_options *to,
		enum colour_mode colour_mode, int lines_to_print, int userspecified_cols) {
	init_ncurses ();
	atexit (close_ncurses);
	tui *t = set_up_tui (cmd, colour_mode, lines_to_print, userspecified_cols);

	// Don't put the print_ctx in the tui struct. Only because libnote
	// passes it, and I don't want to fuck around with shadowed
	// variables.
	struct tsk_print_ctx pc = {
		.user = t,
		.print = tui_tsk_print,
		.msg = tui_tsk_msg,
		.colour = tui_colour,
		.confirm = tui_confirm,
		.multi_confirm = tui_multi_confirm,
		.input = tui_input,
	};

	to->flags |= TSK_FLAG_ALWAYS_PRINT_ITEMS;
	to->prog_name = nullptr;
	t->tsk_ctx = *tsk_ctx;
	ASSERT (!tsk_update_ctx (&t->tsk_ctx, false, &pc, to));

	set_pc_cols (&pc, t->userspecified_cols);

	bool notes_were_updated;
	if (tsk_do_cmd (&t->tsk_ctx, cmd, &notes_were_updated) == -1)
		ASSERT (!"Put in proper error here");

	tsk_run_autocmds (&t->tsk_ctx, cmd, &notes_were_updated);

	// Need to clear the window the first time.
	erase_widgets (t->widgets);

	draw (t);
	for (;;) {
		if (input (t) == 2)
			break;
		update ();
		draw (t);
	}
	tsk_free_cmd (cmd);

	exit (0);
}
