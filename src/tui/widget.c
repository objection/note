#define _GNU_SOURCE

#include "misc.h"
#include "draw.h"
#include "widget.h"
#include "widgets/items.h"
#include "widgets/multi-confirm.h"
#include "widgets/cmdline-window.h"
#include "widgets/completion.h"
#include "widgets/cmdline.h"
#include "widgets/keys.h"
#include "widgets/msg.h"
#include "widgets/input.h"

static widget_op_mode *get_widgets_only_op_mode (widget *w) {
	return w->modes.d + 0;
}

static void maybe_resize_pad_to_new_cols (WINDOW *win) {
	int h, w;
	getmaxyx (win, h, w);
	if (w < COLS)
		wresize (win, h, COLS);
}

void resize (struct tui *t) {
	erase_widgets (t->widgets);
	set_pc_cols (t->tsk_ctx.pc, t->userspecified_cols);

	// Make sure the pads are at least COLS wide. I don't think it's
	// necessary to do anything about the height. The pads are
	// extended vertically as we draw.
	EACH (widget, countof (t->widgets->arr), t->widgets->arr) {
		maybe_resize_pad_to_new_cols (widget->win);
		if (widget->statusline_widget)
			maybe_resize_pad_to_new_cols (widget->statusline_widget->win);
	}
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	draw (t);
}

// Only for items right now.
void highlight_selected_item (struct tui *t, struct widget *w,

		// This is another quick hack. Maybe it's fine. This it the
		// colour we're to set the line you just left to.
		enum tsk_colour last_colour) {

	int last_cursor_line = w->fns->elem_tl_from_cursor (t, w->last_cursor).y;
	WINDOW *win = w->win;

	wmove (win, last_cursor_line, 0);
	mvwchgat (win, last_cursor_line, 0, -1, A_NORMAL, last_colour, nullptr);
	int cursor_line = w->fns->elem_tl_from_cursor (t, w->cursor).y;
	mvwchgat (win, cursor_line, 0, -1, A_BOLD, 0, 0);
}

void increment_cursor (tui *t, widget *w) {
	struct vec *cursor = &w->cursor;
	if (cursor->y < w->fns->get_element_count (t, w) - 1)
		cursor->y++;
}

void decrement_cursor (tui *t, widget *w) {
	struct vec *cursor = &w->cursor;
	if (cursor->y > w->fns->get_idx_of_first_element (t, w))
		cursor->y--;
}

default_op make_default_op (default_op_fn *fn, char *short_description) {
	default_op r = {
		.short_description = short_description,
		.fn = fn,
	};
	return r;
}

void push_op (struct op_arr *op_arr, char *short_description, int n_keys, int *keys,
		int n_args, union op_args args[n_args], op_fn *fn) {
	ASSERT (n_keys <= MAX_OP_KEYS);
	struct op op = {.n_keys = n_keys, .fn = fn, .short_description = short_description};
	memcpy (op.keys, keys, sizeof *keys * n_keys);
	if (n_args)
		memcpy (op.args, args, sizeof *args * n_args);

	char *p = op.keys_as_str;
	for (int i = 0; i < op.n_keys; i++) {
		const char *key_as_str = unctrl (op.keys[i]);
		p = memccpy (p, key_as_str, 0,

				// I'm allowing myself to copy more then the max so
				// that I can ASSERT if it goes over. Maybe not wise.
				sizeof op.keys_as_str);
		ASSERT (p - op.keys_as_str < (int) sizeof op.keys_as_str - 1);

		// memccpy puts you *after* dest, after the 0 in this case.
		p--;
	}

	*p = 0;
	op.keys_as_str_len = p - op.keys_as_str;
	arr_add (op_arr, op);
}

void go_to_a_cmdline (tui *t, widget *cmdline_widget, enum editbuf_id editbuf_id) {

	t->widgets->current = cmdline_widget;
	t->editbufs.id_current = editbuf_id;
}

void insert_word_into_editbuf (struct editbuf *editbuf, int completion_word_start,
		char *word,

		// The cmdline widget, presumably, though there could be other
		// widgets that use editbufs. Probably if we were to allow
		// editing notes directly each note would be a widget. But
		// would each part of the note widget be a widget?
		widget *w) {
	int word_len = strlen (word);
	int to_splice_len =

		// The assuption is that there's only one line.
		w->cursor.x - completion_word_start;
	arr_splice (editbuf, completion_word_start, to_splice_len);
	arr_insert_mul (editbuf, completion_word_start, word, word_len);
	w->cursor.x = completion_word_start + word_len;
}

void make_new_cmdline_and_go_to_it (tui *t, enum editbuf_id editbuf_id, bool forwards) {

	go_to_a_cmdline (t, &t->widgets->as.cmdline, editbuf_id);

	ASSERT (cur_widget (t) == &t->widgets->as.cmdline);

	auto w = cur_widget (t);
	cur_editbuf (t)->n = w->cursor.x = 0;

	// I'm using cmd.c's tokens. Most of them are going to be
	// represented in the t; ":" won't.
	cur_editbuf (t)->forwards = forwards;
}

void refresh_widget (widget *w) {
	pnoutrefresh (w->win,
			w->pad_tl.y,
			w->pad_tl.x,
			w->screen_rect.tl.y,
			w->screen_rect.tl.x,
			w->screen_rect.br.y,
			w->screen_rect.br.x);
}

widget_op_mode *get_widget_mode (widget *w) {
	if (!w->get_op_mode (w))
		return w->modes.d + 0;
	auto r = w->get_op_mode (w);
	ASSERT (r);
	return r;
}

void erase_widgets (struct widgets *widgets) {
	EACH (_, countof (widgets->arr), widgets->arr) {
		if (_->win)
			werase (_->win);
	}
}

void draw_cmdlinelike_widget (struct tui *t, widget *w, char *chars_to_print_at_left) {
	werase (w->win);
	auto editbuf = cur_editbuf (t);

	// Like the ":" at the start of the cmdline, or the "/" at the
	// start of the search cmdline.
	mvwprintw (w->win, 0, 0, "%s", chars_to_print_at_left);

	// The %.*s thing simply doesn't work with ncurses, so you need to
	// do this or something else.
	for (size_t i = 0; i < editbuf->n; i++)
		waddch (w->win, editbuf->d[i]);
}


widget_op_modes make_widget_op_modes (int n_widget_mode_arr, widget_op_mode *widget_mode_arr) {
	widget_op_modes r = {
		.d = calloc (n_widget_mode_arr, sizeof *r.d),
	};
	for (int i = 0; i < n_widget_mode_arr; i++)
		r.d[r.n++] = widget_mode_arr[i];
	return r;
}

widgets *set_up_widgets (tui *t) {
	widgets *r = malloc (sizeof *r);
	*r = (widgets) {
		.as = {
			.items = make_items_widget (t),
			.multi_confirm = make_multi_confirm_widget (t),
			.cmdline_window = make_cmdline_window_widget (t),
			.cmdline = make_cmdline_widget (t),
			.completion = make_completion_widget (t),
			.keys = make_keys_widget (t),
			.msg = make_msg_widget (t),
			.input = make_input_widget (t),
		},
	};

	EACH (widget, countof (r->arr), r->arr) {
		ASSERT (!widget->win);
		widget->win = newpad (1, COLS);
		if (widget->statusline_widget)
			widget->statusline_widget->win = newpad (1, COLS);

		if (!widget->get_op_mode)
			widget->get_op_mode = get_widgets_only_op_mode;

		EACH (mode, widget->modes.n, widget->modes.d) {
			for (size_t i = 0; i < mode->ops.n; i++)
				ASSERT (mode->ops.d[i].short_description &&
						strlen (mode->ops.d[i].short_description) < MAX_KEY_DESCRIPTION);

			if (mode->default_op.fn)
				ASSERT (mode->default_op.short_description);
		}

		ASSERT (widget->description);

		// Forbid statusline widgets with statusline widgets.
		ASSERT (!widget->statusline_widget ||
				!(widget->statusline_widget->statusline_widget));
	}
	r->current = &r->as.items;
	return r;
}
