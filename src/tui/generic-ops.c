#include "generic-ops.h"
#include "misc.h"
#include "widget.h"
#include "cmd.h"
#include <rab.h>

// These ones are generic.
MAKE_OP_FUNC (increment_cursor) {
	increment_cursor (t, w);
	return 0;
}

// These ones are generic.
MAKE_OP_FUNC (decrement_cursor) {
	decrement_cursor (t, w);
	return 0;
}

// This is for <c-d>, <c-u>, <c-f>. and <c-b>.
// In Vim, <c-f> and <c-d> behave a little differently:
// 	  <c-d> seems to move the cursor and the screen at the same rate
// 	  		So the cursor stays in place in screen space.
// 	  <c-f> seems to scroll the whole screen then make sure the cursor
// 	  		remains on screen.
//
// I'll have them both work the same way, the <c-f> way. I don't think
// the <c-d> way would work in a non-confusing way, bearing in mind
// notes can be any length.
static void scroll_screen_down (tui *t, widget *w, int amount) {

	w->pad_tl.y += amount;

	if (w->fns->elem_tl_from_cursor (t, w->cursor).y < w->pad_tl.y)
		w->cursor = w->fns->cursor_y_from_pad_line (t, w->pad_tl.y);
}

//
// This is for <c-d>, <c-u>, <c-f>. and <c-b>.
//
// In Vim, <c-f> and <c-d> behave a little differently, but they won't
// here. They scroll the screen, then make sure the cursor is on the
// screen.
//
static void scroll_screen_up (tui *t, widget *w, int amount) {
	auto elem_end = w->fns->elem_br_from_cursor;

	w->pad_tl.y -= amount;
	int screen_height = w->screen_rect.br.y - w->screen_rect.tl.y;
	if (w->pad_tl.y < 0)
		w->pad_tl.y = 0;

	while (elem_end (t, w->cursor).y > w->pad_tl.y + screen_height)
		op_decrement_cursor (nullptr, t, w);
}

MAKE_OP_FUNC (c_e) {
	scroll_screen_down (t, w, 1); return 0;
}

MAKE_OP_FUNC (c_y) {
	scroll_screen_up (t, w, 1);
	return 0;
}

MAKE_OP_FUNC (c_f) {
	int screen_height = w->screen_rect.br.y - w->screen_rect.tl.y;
	scroll_screen_down (t, w, screen_height);
	return 0;
}

MAKE_OP_FUNC (c_b) {
	int screen_height = w->screen_rect.br.y - w->screen_rect.tl.y;
	scroll_screen_up (t, w, screen_height);
	return 0;
}

MAKE_OP_FUNC (c_d) {
	int screen_height = w->screen_rect.br.y - w->screen_rect.tl.y;
	scroll_screen_down (t, w, screen_height / 2);
	return 0;
}

MAKE_OP_FUNC (search) {
	tui_search (t, op_args[0].b);
	return 0;
}

MAKE_OP_FUNC (c_u) {
	int screen_height = w->screen_rect.br.y - w->screen_rect.tl.y;
	scroll_screen_up (t, w, screen_height / 2);
	return 0;
}

MAKE_OP_FUNC (redraw) {
	t->flags |= TF_RECREATE_WINDOW_CONTENTS;
	return 0;
}

MAKE_OP_FUNC (gg) {
	w->pad_tl.y = 0;
	w->cursor = (struct vec) {};
	return 0;
}

MAKE_OP_FUNC (G) {
	w->cursor.y = w->fns->get_element_count (t, w) - 1;
	return 0;
}

MAKE_OP_FUNC (change_mode_to_normal) {
	t->widgets->current = &t->widgets->as.items;
	return 0;
}

MAKE_OP_FUNC (change_mode_to_cmdline_win) {
	t->widgets->current = &t->widgets->as.cmdline_window;
	w = cur_widget (t);
	w->cursor.y = t->n_cmd_history - 1;
	w->cursor.x = 0;
	w->pad_tl.y = t->n_cmd_history - 1 - w->required_screen_wh.y;
	return 0;
}

MAKE_OP_FUNC (change_mode_to_cmdline) {
	make_new_cmdline_and_go_to_it (t, op_args[0].i, op_args[1].b);
	return 0;
}

MAKE_OP_FUNC (go_to_parent_widget) {
	t->widgets->current = w->parent_widget;
	return 0;
}

MAKE_OP_FUNC (toggle_keys) {
	t->show_keys_widget = !t->show_keys_widget;
	return 0;
}

void push_generic_screenmoving_ops (struct op_arr *out) {
	PUSH_NO_ARGS_OP (out, "Scroll down",    op_c_e, TUI_CTRL ('e'));
	PUSH_NO_ARGS_OP (out, "Scroll up",      op_c_y, TUI_CTRL ('y'));
	PUSH_NO_ARGS_OP (out, "Down half page", op_c_d, TUI_CTRL ('d'));
	PUSH_NO_ARGS_OP (out, "Up half page",   op_c_u, TUI_CTRL ('u'));
	PUSH_NO_ARGS_OP (out, "Down 1 page",    op_c_f, TUI_CTRL ('f'));
	PUSH_NO_ARGS_OP (out, "Up 1 page",      op_c_b, TUI_CTRL ('b'));

	// Can't quite put gg and G here because in the msg window we
	// can't say "Go to last item". We could just say "Go to bot". Or
	// we could abandon thie push_generic_screenmoving_ops.
}

MAKE_OP_FUNC (delete_to_start) {
	ASSERT (w == &t->widgets->as.cmdline || w == &t->widgets->as.input);
	auto editbuf = cur_editbuf (t);
	editbuf->n = 0;
	cur_widget (t)->cursor.x = 0;
	return 0;
}

MAKE_OP_FUNC (delete) {
	ASSERT (w == &t->widgets->as.cmdline || w == &t->widgets->as.input);
	if (w->cursor.x > 0)
		arr_splice (cur_editbuf (t), --w->cursor.x, 1);
	return  0;
}

MAKE_OP_FUNC (delete_word_behind) {
	ASSERT (w == &t->widgets->as.cmdline || w == &t->widgets->as.input);
	auto editbuf = cur_editbuf (t);
	int idx = w->cursor.x - 1;
	if (idx <= 0)
		return 0;
	while (idx >= 0 && !iskeyword (editbuf->d[idx]))
		idx--;
	while (idx >= 0 && iskeyword (editbuf->d[idx]))
		idx--;
	if (!iskeyword (editbuf->d[idx]))
		idx++;

	if (idx <= 0)
		idx = 0;

	arr_splice (editbuf, idx, w->cursor.x - idx);
	w->cursor.x = idx;
	return  0;
}

MAKE_DEFAULT_OP (self_insert) {
	ASSERT (w == &t->widgets->as.cmdline || w == &t->widgets->as.input);
	arr_insert_extending (cur_editbuf (t), (size_t) w->cursor.x, &ch);
	w->cursor.x++;
}

