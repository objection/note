#include "misc.h"
#include "config.h"
#include <ncurses.h>
#include <ctype.h>

void set_pc_cols (tsk_print_ctx *pc, int userspecified_cols) {
	pc->cols = userspecified_cols != -1
		? userspecified_cols
		: PRINT_MAX_COLS;

	if (pc->cols > COLS)
		pc->cols = COLS;

	// NOTE: this -= 1 is a LOAD-BEARING -= 1.
	//
	// That's a joke. What I'm saying is I feel like I shouldn't have
	// to write - 1. It may be a bug in wrap.
	//
	// The reason I have to write it is ncurses prints nothing if
	// *cols == COLS. I still think it's wrap's fault. This bug
	// puzzled me for about two hours of hell.
	pc->cols -= 1;
}

int create_notefile_stats () {
#if 0
	struct tsk_notefiles *notefiles = &args.to->notefiles;
	args.notefile_stats.n = 0;
	FORI (i, ctx->notefiles->n) {
		struct stat statbuf = {};
		stat (ctx->notefiles->d[i].str, &statbuf);

		// It doesn't matter if stat fails. Well, we probably should
		// check for some error. But if the file doesn't exist it
		// doesn't matter. Files *don't* exist in the tui until you
		// write them.
		arr_add (&args.notefile_stats, statbuf);
	}
#endif
	return 0;
}

bool iskeyword (char ch) {
	/* @,48-57,_,192-255, */
	return isalpha (ch) || (ch >= 48 && ch <= 57) ||
		ch == '_' ||

		// We ignore >= 192 and <= 255 because we're using
		// signed chars.
		/* (ch >= 192 && ch <= 255) || */
		ch == '@';
}
