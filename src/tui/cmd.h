#pragma once
#include "tui.h"

int tui_search (struct tui *t, bool flip);
int tui_split_cmdline_and_do_cmd (struct tui *t);
int tui_do_cmd (struct tui *t, struct tsk_strs *cmd_words, bool replace_cmd_in_cmd_list);
void print_err (struct tui *t, char *fmt, ...);
int push_editbuf (struct editbuf *editbuf);
