#define _GNU_SOURCE

#include "tui.h"
#include "cmd.h"
#include "../macros.h"
#include "widget.h"
#include "misc.h"
#include <rab.h>
#include <ctype.h>

// This is for printing errors. It's not tui_tsk_print even though
// that function, which is similar. That's tsk's callback. I'd have
// the two function share a common function but you don't save many
// lines doing that in C. You always have to do the whole "va_list ap;
// ..." bit. I think I should start using macros for these.
void print_err (struct tui *t, char *fmt, ...) {
	auto w = &t->widgets->as.msg;
	werase (w->win);
	wmove (w->win, 0, 0);

	va_list ap;
	va_start (ap);
	char buf[BUFSIZ];
	vsnprintf (buf, sizeof buf, fmt, ap);
	va_end (ap);

	waddstr (w->win, buf);
}

static struct tsk_strs get_tsk_strs_through_not_wordexp (char *str, struct tui *t) {

	struct tsk_strs r = {};
	r.d = rab_shell_split (str, (ssize_t *) &r.n);
	r.a = r.n;

	if (!r.n) {
		print_err (t, "Couldn't split cmdline \"%s\"", str);
		return (struct tsk_strs) {};
	}
	return r;
}

// Libnote has no need need for the concept of, eg, a "cursor", so we
// need to turn references to, eg, cursors to indexes numbers.
static void transform_tui_cmd_words_to_tsk_ones (ssize_t n_cmd_words, char **cmd_words,
		struct tui *t) {
	auto items_widget = &t->widgets->as.items;

	FORI (i, n_cmd_words) {
		if (!strcmp (cmd_words[i], ".")) {
			free (cmd_words[i]);
			asprintf (&cmd_words[i], "n%d", items_widget->cursor.y);
		}
	}
}

#if 0
static void tui_cmd_ls (struct tui *t) {
	auto w = &t->widgets->as.cmdline;
	wmove (w->win, 0, 0);
	werase (w->win);
	$each (_, t->cmds.d, t->cmds.n)
		wprintw (w->win, "%zu: %s\n", _ - t->cmds.d, _->text);
	wprintw (w->win, "Press ENTER or type command to continue\n");
	pnoutrefresh (w->win, 0, 0,
			LINES - (t->cmds.n + 1), 0, LINES - 1, COLS - 1);
	t->this_thing_n_lines = t->cmds.n + 1;
	t->flags |= TF_ERROR_MSG;
}
#endif

int tui_get_bufnr (struct tui *t, char *str_cmd, int *out) {
	if (!*str_cmd)
		return t->cmds.idx;
	struct tsk_strs p = get_tsk_strs_through_not_wordexp (str_cmd, t);
	if (!p.n)
		return -1;

	int r = 1;
	struct partiallymatched_ops matches;

	// Numbers are got from isdigit, hence -1 will be interpreted as a
	// pattern. This is how Vim does it.
	if (isdigit (*p.d[0])) {
		if (sscanf (p.d[0], "%d", &r) == EOF)
			print_err (t, "Not a number; shouldn't happen; a bug");
		if (r > (int) t->cmds.n - 1)
			print_err (t, "Buffer %s doesn't exist", p.d[0]);

	} else {
		char buf[0xfff];

		if ($get_regmatching_members (p.d[0], t->cmds.n, t->cmds.d,
				text, buf, &matches))
			WARN_GOTO_OUT ("Regmatch \"%s\" failed", p.d[0]);
		else if (!matches.n)
			WARN_GOTO_OUT ("No matching cmd for \"%s\"", p.d[0]);
		else if (matches.n > 1)
			WARN_GOTO_OUT ("More than one match for \"%s\"", p.d[0]);
		else
			*out = matches.d[0];
		free (matches.d);
	}
out:;
	arr_each (&p, _)
		free (*_);
	free (p.d);
	if (matches.n)
		free (matches.d);
	return r;
}

#if 0
static int tui_cmd_buffer (struct tui *t, char *str_cmd) {
	int bufnr = tui_get_bufnr (t, str_cmd);
	if (bufnr == -1)
		return 1;
	t->cmds.idx = bufnr;
	return 0;
}
#endif

#if 0
static int tui_cmd_bdelete (struct tui *t, char *str_cmd) {
	int bufnr = tui_get_bufnr (t, str_cmd);
	if (bufnr == -1)
		return 1;
	tsk_free_cmd (&t->cmds.d[bufnr]);
	arr_splice (&t->cmds, bufnr, 1);
	if (t->cmds.n == 0)
		arr_push (&t->cmds, (struct tsk_cmd) {});
	else
		t->cmds.idx = t->cmds.n -1 ;
	return 0;
}
#endif

// This just cmps the statements the lists. It's all that matters for
// the t.
int cmp_cmds (const void *_a, const void *_b) {
	const struct tsk_cmd *a = _a;
	const struct tsk_cmd *b = _b;

	int val = 0;

	// To be honest, this is maybe all we should consider. cmping the
	// whole cmd is a pretty tall order. You can see that I was
	// trying to compare all the bits: the number of statements, the
	// number of items in the list, etc, but to do it properly you'd
	// need to compare _everything_. Comparing the string will cover
	// all those bases, right?
	if ((val = strcmp (a->text, b->text)) != 0)
		return val;
	return 0;
}

struct tsk_strs get_sundered_cmdline (struct tui *t) {
	struct tsk_strs r = {};
	auto editbuf = cur_editbuf (t);
	arr_add (editbuf, 0);
	r = get_tsk_strs_through_not_wordexp (editbuf->d,  t);
	if (!r.n)
		goto out;
out:
	arr_pop (editbuf);
	return r;
}

static int tui_parse_cmd (struct tui *t,

		// Is a pointer because it gets mutated.
		struct tsk_strs *cmd_words, tsk_cmd *out) {

	int r = 1;
	auto cmd = cur_cmd (t);

	transform_tui_cmd_words_to_tsk_ones (cmd_words->n, cmd_words->d, t);

	*out = tsk_parse_cmd (&t->tsk_ctx, cmd_words->d, cmd_words->n, cmd->source,
			cur_cmd (t));

	if (out->flags & TCF_FAILED) {
		tsk_free_cmd (out);
		return 1;
	}

	if (!(out->flags & TCF_NEW_VIEW_DISCARDED_OLD_VIEW)) {
		auto prev_cmd = arr_last (&t->cmds);
		FORI (i, prev_cmd->view_words.n)
			arr_insert (&out->view_words, i, prev_cmd->view_words.d[i]);
	}

	r = 0;
	create_notefile_stats ();

	out->user = CALLOC_1 (sizeof (struct vec));
	return r;
}

int push_editbuf (struct editbuf *editbuf) {

	arr_add (editbuf, 0);

	// Add to prev bufs.
	//
	// Don't push the str if it's the same one, ie most likely
	// you've done 'n'.
	if (!editbuf->n_prevs
			|| (strcmp (editbuf->d, editbuf->prevs[editbuf->n_prevs - 1]))) {
		if (editbuf->n_prevs == MAX_HISTORY - 1) {
			memcpy (editbuf->prevs, editbuf->prevs + 1,
					sizeof *editbuf->prevs * MAX_HISTORY - 1);
		}
		editbuf->prevs[editbuf->n_prevs] = strdup (editbuf->d);
		if (editbuf->n_prevs < MAX_HISTORY)
			editbuf->n_prevs++;
	}
	editbuf->n = 0;
	return 0;
}

// We search by going through the "front door" of Libnote's cmd.c,
// that is constructing a filter. The reason we do this and don't
// iterate more directly is a) I have this desire to make Libnote kind
// of sacrosanct -- you know, only exposing a few functions -- and b)
// Doing it like this is simpler -- for libnote. It's really just the
// error messages it makes easier. I was having a terrible time with
// null global variables. Well, maybe I'll use fewer globals next time
// around. But I'm happy to do it this way unless it proves awful in
// some other way. When you funnel everything through the same tunnel
// everything is consistent.
int tui_search (struct tui *t, bool flip) {

	int r = 1;

	auto editbuf = &t->editbufs.d[EID_SEARCH];
	auto cmd_words = get_sundered_cmdline (t);
	bool used_old_cmd = 0;
	if (!cmd_words.n) {
		if (editbuf->n_prevs) {
			char *last_cmd = editbuf->prevs[editbuf->n_prevs];
			if (!last_cmd) {
				print_err (t, "No previous search");
				goto out;
			}
			cmd_words = get_tsk_strs_through_not_wordexp (last_cmd, t);
			if (!cmd_words.n) {
				print_err (t, "A programming error, I think");
				goto out;
			}
			used_old_cmd = 1;
		}
	}
	auto cursor = &t->widgets->as.items.cursor;
	bool forwards = editbuf->forwards;

	if (flip)
		forwards = !forwards;

	// NOTE (trickiness): search figures out whether to go forwards
	// from editbuf->forwards.
	/* if (op_args[0].b == 1) */
	/* 	forwards = !forwards; */

	if (forwards) {
		arr_insert (&cmd_words, 0, rab_get_str ("n%d..", cursor->y + 1));
		arr_add (&cmd_words, strdup ("n0"));
	} else {
		arr_insert (&cmd_words, 0, rab_get_str ("0..%d", cursor->y - 1));
		arr_add (&cmd_words, strdup ("n-1"));
	}

	struct tsk_cmd cmd;
	if (tui_parse_cmd (t, &cmd_words, &cmd))
		goto out;

	// We'll let you do the cmd, just do see if it's useful. It means
	// presumably you could do "//this R". You'd move to the match and
	// remove it. You'll also be allowed do to this: "//this \;
	// /that". That would leave you on the first that.
	bool notes_were_updated;
	if (tsk_do_cmd (&t->tsk_ctx, &cmd, &notes_were_updated))
		goto out;

	int item_idx = arr_last (&cmd.statements)->view->d[0].item_idx;
	if (forwards) {
		for (int i = cursor->y; i < (int) t->tui_item_view.n; i++) {
			if (t->tui_item_view.d[i].item_idx == item_idx) {
				cur_widget (t)->cursor.y = i;
				break;
			}
		}
	} else {
		for (int i = cursor->y - 1; i >= 0; i--) {
			if (t->tui_item_view.d[i].item_idx == item_idx) {
				cur_widget (t)->cursor.y = i;
				break;
			}
		}
	}
	r = 0;
out:

	// Push editbuf basically regardless.
	if (!used_old_cmd)
		push_editbuf (editbuf);
	return r;
}

static void tui_free_tsk_cmd (tsk_cmd *cmd) {
	free (cmd->user);
	tsk_free_cmd (cmd);
}

int tui_do_cmd (struct tui *t,

		// Is a pointer because it gets mutated.
		struct tsk_strs *cmd_words, bool replace_cmd_in_cmd_list) {

	struct tsk_cmd new_cmd;
	if (tui_parse_cmd (t, cmd_words, &new_cmd))
		return 1;

	bool notes_were_updated;

	if (tsk_do_cmd (&t->tsk_ctx, &new_cmd, &notes_were_updated))
		return 1;

	tsk_run_autocmds (&t->tsk_ctx, &new_cmd, &notes_were_updated);

	if (replace_cmd_in_cmd_list) {
		tui_free_tsk_cmd (cur_cmd (t));
		*cur_cmd (t) = new_cmd;
	} else {
		arr_insert (&t->cmds, t->cmds.idx + 1, new_cmd);
		t->cmds.idx++;
	}

	// We always make a new view, so just set these to zero.
	t->widgets->as.items.pad_tl.y = 0;
	t->widgets->as.items.cursor = (struct vec) {};

	t->flags |= TF_RECREATE_WINDOW_CONTENTS;

	return 0;
}

int tui_split_cmdline_and_do_cmd (struct tui *t) {

	int r = 1;

	auto cmd_words = get_sundered_cmdline (t);
	if (!cmd_words.n)
		goto out;

	int rc = tui_do_cmd (t, &cmd_words, 0);
	if (!rc)
		r = 0;

out:
	push_editbuf (cur_editbuf (t));

	return r;
}
