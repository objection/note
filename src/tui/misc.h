#pragma once

#include <libnote/libnote.h>

void set_pc_cols (tsk_print_ctx *pc, int userspecified_cols);
int create_notefile_stats ();
bool iskeyword (char ch);
