#include "widget.h"

// This is the height as it is on the screen, ie, nothing to
// do with the pad.
enum { CMDLINE_WIN_HEIGHT = 7 };

static void set_tiled_widgets_screen_rect (widget *w, int one_past_widget_bottom) {

	w->screen_rect = (rect) {
		.br = {
			.y = one_past_widget_bottom - 1,
			.x = COLS - 1,
		},
	};

	if (w->required_screen_wh.y) {
		w->screen_rect.tl.y = w->screen_rect.br.y

			// The brs, the bottom rights in this program are
			// inclusive, so if your widget takes up one line the .ys
			// of its tl and br will be the same, hence we add one
			// here.
			+ 1 -

			w->required_screen_wh.y;
		if (w->screen_rect.tl.y < 0)
			w->screen_rect.tl.y = 0;
	}
}

static void set_popup_widgets_screen_rect (widget *w) {
	w->screen_rect = (rect) {
		.tl = w->specified_tl,
		.br = {

			// -1 because bottom-rights in this program are inclusive.
			.y = w->specified_tl.y + w->required_screen_wh.y - 1,
			.x = w->specified_tl.x + w->required_screen_wh.x - 1,
		},
	};
}

static void draw_widget (tui *t, widget *w, int *one_past_widget_bottom);

static void draw_statusline (tui *t, widget *statusline_w,
		int *one_past_widget_bottom) {

	wmove (statusline_w->win, 0, 0);

	// This function mostly just draws the statusline as usual, but
	// before it does it turns on reverse video. It shouldn't actually
	// be necessary to do this; we could do it only once at the start.
	// But it doesn't hurt. Maybe it does. Maybe it masks small bugs.
	werase (statusline_w->win);

	// NOTE: I could only clear the second line and save drawing this.
	mvwhline (statusline_w->win, 0, 0, ' ' | A_REVERSE, 400);
	wattr_on (statusline_w->win, A_REVERSE, nullptr);

	t->widget_we_are_printing_to = statusline_w;

	draw_widget (t, statusline_w, one_past_widget_bottom);

	// Not setting the attrs off because this function is saying that
	// statuslines are always in reverse video.
}

static void draw_widget (tui *t, widget *w, int *one_past_widget_bottom) {

	if (w->statusline_widget)
		draw_statusline (t, w->statusline_widget, one_past_widget_bottom);

	t->widget_we_are_printing_to = w;
	w->draw (t, w, *one_past_widget_bottom);

	if (!w->is_popup)
		set_tiled_widgets_screen_rect (w, *one_past_widget_bottom);
	else
		set_popup_widgets_screen_rect (w);
	refresh_widget (w);

	if (!w->is_popup)
		*one_past_widget_bottom = w->screen_rect.tl.y;
}

static int fix_view (struct tui *t, struct widget *w) {

	int elem_start_line = w->fns->elem_tl_from_cursor (t, w->cursor).y;
	int elem_end_line = w->fns->elem_br_from_cursor (t, w->cursor).y;
	int item_height = (elem_end_line - elem_start_line) + 1;

	int screen_height = w->screen_rect.br.y - w->screen_rect.tl.y;
	if (elem_start_line < w->pad_tl.y)
		w->pad_tl.y = elem_start_line;

	else if (elem_start_line >= w->pad_tl.y + screen_height ||
			w->cursor.y == w->fns->get_element_count (t, w) - 1) {

		if (item_height > screen_height)
			w->pad_tl.y = elem_start_line;

		else {
			w->pad_tl.y = elem_end_line - screen_height - 1;

			// This is a bit of a hack. It's possible for w->pad_tl.y
			// to end up < 0 if w->cursor.y is 0, and probably in
			// other situations. Probably this will just take care of
			// it. But it also might fuck me in the arse.
			if (w->pad_tl.y < 0)
				w->pad_tl.y = 0;
		}
	}
	return 0;
}

static void draw_cursor (tui *t, widget *w) {

	// Draws the cursor, which seems to necessitate moving the cursor.
	// In ncurses the curses moves as you print, which I suppose is
	// OK. It means that you'll know where you've just been. I'm
	// tracking that myself. I think I could have used getparyx and
	// stuff like that instead.
	vec pad_line_start = w->fns->elem_tl_from_cursor (t, w->cursor);

	// This is temporary thing, OK? When you're typing the cursor
	// needs to be one past the cursor. There's various ways we could
	// do this, and various ways we could make this generic. We could
	// call a get_cursor function. We could store an x_offset in the
	// widget. We could -- and this might be the best one -- make the
	// cmdline's cursor just always be one to the right. That sounds
	// fiddly, though. We could have a ARE_TYPING flag. We could use a
	// IS_TYPING_WIDGET flag. But until we have reason to get really
	// generic let's not do any of those things. Premature genericity
	// is the root some evil.
	int x_offset = 0;
	if (w == &t->widgets->as.cmdline || w == &t->widgets->as.input)
		x_offset++;
	wmove (w->win, pad_line_start.y, w->cursor.x + x_offset);

	// Necessary, else you won't see it.
	refresh_widget (w);
}

void draw (struct tui *t) {

	auto w = t->widgets->current;
	int one_past_widget_bottom = LINES;

	if (w == &t->widgets->as.input)
		draw_widget (t, w, &one_past_widget_bottom);
	else
		draw_widget (t, &t->widgets->as.cmdline, &one_past_widget_bottom);

	if (t->show_keys_widget)
		draw_widget (t, &t->widgets->as.keys, &one_past_widget_bottom);

	if (w == &t->widgets->as.input || w == &t->widgets->as.msg)
		draw_widget (t, &t->widgets->as.msg, &one_past_widget_bottom);
	else if (w == &t->widgets->as.cmdline_window)
		draw_widget (t, &t->widgets->as.cmdline_window, &one_past_widget_bottom);

	// I think that draw_items and draw_confirmations should be the
	// same thing. Really both should take structs or both should work
	// with functions. Regardless printing items should be basically
	// printing arbitrary text.
	if (w != &t->widgets->as.multi_confirm)
		draw_widget (t, &t->widgets->as.items, &one_past_widget_bottom);
	else
		draw_widget (t, &t->widgets->as.multi_confirm, &one_past_widget_bottom);

	if (w == &t->widgets->as.completion)
		draw_widget (t, &t->widgets->as.completion,
				&(int) {t->widgets->as.cmdline.screen_rect.tl.y});

	// We could have some flag in the widget instead of this check.
	// Just not sure what the flag would be. These widgets bots are a
	// fixed.
	if (!(
				   w == &t->widgets->as.cmdline
				|| w == &t->widgets->as.completion
				|| w == &t->widgets->as.input))
		fix_view (t, w);

	draw_cursor (t, w);
	doupdate ();
	t->flags &= ~TF_RECREATE_WINDOW_CONTENTS;

	// This will work for now. The messages are sent by Libnote and so
	// we don't have control over it. Luckily I want all such messages
	// to go to the message widget.
	t->widget_we_are_printing_to = &t->widgets->as.msg;
}


