#include "input.h"
#include "widget.h"
#include "draw.h"

int input (struct tui *t) {

	int ch;

	int r = 1;
	bool an_op_was_completed = false;
	auto w = cur_widget (t);
	w->last_cursor = w->cursor;
	/* auto editbuf = cur_editbuf (t); */
	static int str[8] = {};

	widget_op_mode *widget_op_mode = w->get_op_mode (w);

	int key_chain_idx = 0;
	for (; key_chain_idx < MAX_OP_KEYS; key_chain_idx++) {
		while ((ch = wgetch (w->win)) == KEY_RESIZE)

			// Resize draws. We could return 1 or something here and
			// let the outer draw draw. Maybe that's neater. Or maybe
			// this is neater. 2024-04-14T06:39:53: this is simpler
			// anyway. Nothing's going to have changed in the
			// meantime.
			resize (t);

		str[key_chain_idx] = ch;
		bool got_partial_match = 0;

		EACH (op, widget_op_mode->ops.n, widget_op_mode->ops.d) {

			if (intnmatch (str, op->keys, key_chain_idx + 1)) {
				if (key_chain_idx + 1 == op->n_keys) {
					r = op->fn (op->args, t, w);
					an_op_was_completed = true;

					t->partiallymatched_ops.n = 0;
					/* t->flags |= TF_REDRAW; */
					goto out;
				} else {
					arr_add (&t->partiallymatched_ops, op - widget_op_mode->ops.d);
					got_partial_match = 1;
					draw (t);
				}
			}
		}
		if (got_partial_match)
			continue;
		else
			break;
	}
	r = 0;
out:
	if (!an_op_was_completed) {

		if (widget_op_mode->default_op.fn)
			widget_op_mode->default_op.fn (t, w, ch);
		else {

			// If what you typed wasn't an op we want to replay it. We
			// could just call some inner function ourselves but maybe
			// it's not so bad to just ungetch it.
			for (size_t i = 0; i < widget_op_mode->ops.n; i++) {

				// We don't blindly ungetch. If you do that you just
				// keep ungetching.
				if (widget_op_mode->ops.d[i].keys[0] == ch && widget_op_mode->ops.d[i].n_keys == 1) {
					ungetch (ch);
					break;
				}
			}
		}
	}
	return r;
}
