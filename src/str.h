#pragma once

#include "misc.h"
#include <stdlib.h>

typedef struct str str_s;
struct str {
	char *d; ssize_t n, a;
};
typedef struct str_arr str_arr_s;
struct str_arr {
	char **d; ssize_t n, a;
};

#define $get_regmatching_members($pattern, $n_objs, $objs, $member_name, $msg_buf, $out) \
	get_regmatching_members ($pattern, $n_objs, $objs, sizeof *$objs, \
			offsetof (typeof (*$objs), $member_name), $msg_buf, $out)

int get_regmatching_members (const char *pattern, ssize_t n_objs,
		void *objs, ssize_t obj_size, ssize_t member_offset, char *msg_buf,
		struct partiallymatched_ops *out);
bool intnmatch (int *a, int *b, size_t n);
int get_index_of_longest_unambiguous_initial_match (char *str, char **strs, int n_strs);
int count_occurences_of_chars (char *str, char the_char);
void strip (char *s);
bool str_arr_is_equal (str_arr_s *a, str_arr_s *b);
int get_index_of_longest_unambiguous_initial_match (char *str, char **strs, int n_strs);
