#pragma once

#include <stdlib.h>
#include "str.h"
int get_files_in_dir (char *glob_pattern, str_arr_s *buf, char *dir,
		bool full_paths, char *err_buf, size_t n_err_buf);
char *get_editor_or_ask (void);
int get_terminal_cols (void);
