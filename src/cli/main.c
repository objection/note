#define _GNU_SOURCE

#include "../tui/tui.h"
#include "../macros.h"
#include "../sys.h"
#include "../../config.h"
#include "limits.h"
#include "sys/time.h"
#include "args.h"
#include <rab.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <termios.h>
#include <unistd.h>
#include <readlinef.h>
#include <wrap.h>
#include <errno.h>
#include <err.h>

const char *CLI_COLOUR_STRS[TSK_N_COLOUR] = {
	[TSK_COLOUR_OFF] = ANSI_NO_COLOUR,
	[TSK_COLOUR_PRINT_HEADER] = ANSI_CYAN,
	[TSK_COLOUR_CHANGE_HEADER] = ANSI_BRIGHT_YELLOW,
	[TSK_COLOUR_NO_MATCH_HEADER] = ANSI_BRIGHT_MAGENTA,
	[TSK_COLOUR_ERROR_HEADER] = ANSI_RED,
	[TSK_COLOUR_ERROR_INDICATORS] = ANSI_BRIGHT_RED,
};

enum confirmation_action {
	CA_NO,
	CA_YES,
	CA_NO_TO_ALL,
	CA_YES_TO_ALL,
	CA_BACK,
	CA_FORWARD,
	CA_START,
	CA_END,
	CA_RESET,
	CA_HELP,
};

static void seed_random () {
    struct timeval t1;
    gettimeofday (&t1, 0);
    srandom (t1.tv_usec * t1.tv_sec);
}

static int cli_colour (struct tsk_print_ctx *pc) {
	FILE *f = pc->is_error ? stderr : stdout;
    int r = fprintf (f, "%s", CLI_COLOUR_STRS[pc->colour_index]);
	fflush (f);
	return r;
}

static int cli_print (struct tsk_print_ctx *pc, char *fmt, ...) {

	// Necessary because I'm using cli_print for the Libnote print
	// function and the msg function. It's a bit flimsy but it'll
	// work so long as I don't change how libnote works (Libnote sends
	// "begin" and "done" messages at the start and end of the msg
	// function, and when it does that fmt is null.).
	if (!fmt)
		return 0;

	FILE *f = pc->is_error ? stderr : stdout;

	char buf[0xfff];
	va_list ap;
	va_start (ap);
	int r = vsnprintf (buf, sizeof buf, fmt, ap);
	va_end (ap);

	fputs (buf, f);
	return r;
}

static char get_single_char () {
	char r;
	int n_read;

	struct termios orig;
	tcgetattr (STDIN_FILENO, &orig);
	struct termios new = orig;
	new.c_lflag  &= ~ICANON;
	ASSERT (tcsetattr (STDIN_FILENO, TCSAFLUSH, &new) != -1);

	for (;;) {
		while ((n_read = read (STDIN_FILENO, &r, sizeof r)) == -1 && errno != EINTR);
		ASSERT (n_read != -1);
		break;
	}

	tcsetattr (STDIN_FILENO, TCSAFLUSH, &orig);
	return r;
}

static bool cli_confirm (struct tsk_print_ctx *, char *fmt, ...) {
	va_list ap; va_start (ap);
	char *buf;
	int r = 0;
	ASSERT (vasprintf (&buf, fmt, ap) >= 0);
	va_end (ap);

	for (;;) {
		if (*buf) {
			printf ("%s", buf);
			fflush (stdout);
		}
		char ch = get_single_char ();
		putchar ('\n');
		if (ch == 'y') {
			r = 1;
			break;
		} else if (ch == 'n' || ch == 0)
			break;
	}
	return r;
}

static int cli_input (int n_out, char *out, struct tsk_print_ctx *, char *fmt, ...) {
	va_list ap; va_start (ap);
	char *r = readlinef_ap (0, fmt, ap);
	va_end (ap);
	if (!r)
		return 1;

	// Copying more than we need. Also not considering sentinel --
	// BUG.
	memcpy (out, r, n_out * sizeof *r);
	free (r);
	return 0;
}

#if 0
static char *get_input (char *fmt, ...) {
	va_list ap;
	va_start (ap);
	char *r = readlinef_ap (0, fmt, ap);
	va_end (ap);
	return r;
}
#endif

#if 0
static enum confirmation_action cli_confirm_super_locked_change (int confirmations_remaining) {

	char buf[BUFSIZ];
	enum confirmation_action r = 0;
	for (;;) {
		wrap (buf, BUFSIZ, 0, "\
				\"yes\": yes; \"n\": no; \"Y\": yes-to-all; \"N\": no-to-all; \
				\"B\": back; \"s\": go-to-start; \"e\": go-to-end;  %d confirmation%s remaining. ",
				confirmations_remaining, $plural (confirmations_remaining));
		char *resp = get_input ("%s", buf);
		if (!resp)                        exit (1);
		if (!strcasecmp (resp, "yes"))    r = CA_YES;
		if (*resp == 'n')                 r = CA_NO;
		if (*resp == 'Y')                 r = CA_YES_TO_ALL;
		if (*resp == 'N')                 r = CA_NO_TO_ALL;
		if (*resp == 'b')                 r = CA_BACK;
		if (*resp == 'S')                 r = CA_START;
		if (*resp == 'e')                 r = CA_END;
		if (*resp == 'r')                 r = CA_RESET;
		free (resp);
		if (r)
			break;
	}
	return r;
}
#endif

static enum confirmation_action cli_confirm_normal_change (int confirmations_remaining) {
	enum confirmation_action r = -1;
	for (;;) {
		printf ("\
				%d confirmation%s remaining. Keys: ynYNserjk; h for explanation: ",
				confirmations_remaining, $plural (confirmations_remaining));
		fflush (stdout);

		char ch = get_single_char ();
		switch (ch) {
		case 'n': return CA_NO;
		case 'y': return CA_YES;
		case 'N': return CA_NO_TO_ALL;
		case 'Y': return CA_YES_TO_ALL;
		case 's': return CA_START;
		case 'e': return CA_END;
		case 'r': return CA_RESET;
		case 'j': return CA_FORWARD;
		case 'k': return CA_BACK;
		case 'h': return CA_HELP;
		}
		fputc ('\n', stdout);
	}
	return r;
}

static int cli_multi_confirm (tsk_ctx *ctx, tsk_cmd *cmd,
		struct tsk_confirmations *confirmations) {
	bool contains_super_locked = 0, just_printed_new_line = 0;
	arr_each (confirmations, confirmation) {
		just_printed_new_line = 0;
		tsk_print_confirmation (ctx, cmd, confirmation, cli_print);
		enum confirmation_action action = -1;
		int confirmations_remaining = confirmations->d + confirmations->n - confirmation;
		switch (confirmation->type) {
		case TCT_SUPER_LOCK:                contains_super_locked = 1;
							 				[[fallthrough]];
		case TCT_CONFIRM:
		case TCT_LOCK:                      action = cli_confirm_normal_change (confirmations_remaining); break;;
		case TCT_NO_CONFIRMATION_NECESSARY: action = 1; break;
		}

		switch (action) {
		case CA_YES:
		case CA_NO:
			confirmation->user_confirms = action;
			if (confirmation == arr_last (confirmations) && contains_super_locked) {
				for (;;) {
					if (confirmations->n == 1)
						printf ("\nThe item ");
					else
						printf ("\nAt least one of the items ");
					printf ("you confirmed was super-locked: proceed? ");
					fflush (stdout);
					char ch = get_single_char ();
					if (ch == 'y')
						goto out;
					else if (ch == 'n') {
						printf ("\nContinuing");
						fflush (stdout);
						sleep (1);
						confirmation--;
						break;
					}
				}
			}
			break;
		case CA_NO_TO_ALL:
		case CA_YES_TO_ALL:
			while (confirmation < confirmations->d + confirmations->n) {
				confirmation->user_confirms = action - 2;
				confirmation++;
			}
			break;
		case CA_BACK:
			if (confirmation == confirmations->d)
				confirmation--;
			else
				confirmation -= 2;
			break;
		case CA_START:
			confirmation = confirmations->d - 1;
			break;
		case CA_END:
			confirmation = arr_last (confirmations) - 1;
			break;
		case CA_RESET:
			arr_each (confirmations, confirmation)
				confirmation->user_confirms = 0;
			confirmation--;
			break;
		case CA_HELP:
			printf ("\n\
					y: yes\n\
					n: no\n\
					Y: yes to all\n\
					N; no to all\n\
					s: go to start\n\
					e: go last one\n\
					j: go to next without changing choice\n\
					k: go to previous without changing choice\n\
					r: re-set all to no");
			confirmation--;
			break;
		case CA_FORWARD:
			break;

		}
		fputc ('\n', stdout);
		just_printed_new_line = 1;
	}
out:
	if (!just_printed_new_line)
		fputc ('\n', stdout);
	return 0;
}

static int print_items (tsk_ctx *tsk_ctx, tsk_cmd *cmd) {

	arr_each (&cmd->statements, statement) {
		auto view = statement->view;
		arr_each (statement->view, _) {
			tsk_print (tsk_ctx, cmd, *_, _ - view->d, view->props_were_specified,
					statement->action.mods.print_what.val.prop_id);
		}
	}
	return 0;
}

// This function mirrors the List-shortcuts action. The reason it
// stays is you'd expect such an option to be a cmdline option, and
// you'd expect the program to exit after that. Probably. Maybe I'm
// just loath to get rid of my nine lines of gold.
static void list_shortcuts (char *data_dir) {
	str_arr_s files = {};
	char err_buf[PATH_MAX];
	int ret = get_files_in_dir ("*.nxt", &files, data_dir, 1, err_buf, PATH_MAX);
	if (ret == -1)
		err (1, "Couldn't get list of shortcut notefiles: %s", err_buf);
	EACH (file, files.n, files.d)
		printf ("%s\n", *file);
}

void complete (tsk_ctx *tsk_ctx, char *cmd_str) {

	tsk_completions tsk_completions;
	tsk_get_completions_from_str (tsk_ctx, cmd_str, nullptr, &tsk_completions);
	EACH (match, tsk_completions.count, tsk_completions.matches) {

		// This is a small hack to work around the fact that Zsh will
		// turn this into a '\"', a backslash-quote. Probably Bash
		// does the same. I'm sure there's a way. Though I'm pretty
		// convinced now the only way to do really nice completion in
		// Zsh is to actually do it in Zsh.
		if (!strcmp (match->str, "\""))
			continue;
		printf ("%s\n", match->str);
	}
}

int main (int argc, char **argv) {

	// This I have just discovered. It turns off FILE locking.  It
	// means stdio functions go fast. Of course, we'll never notice
	// the difference. And I really don't see any difference, having
	// looked at the program in kcachegrind.
	__fsetlocking (stdout, FSETLOCKING_BYCALLER);

	seed_random ();

	args args = make_args (argc, argv);

	struct tsk_print_ctx pc = {
		.print = cli_print,
		.msg = cli_print,
		.cols = your_default_wrap_opts.cols,
		.lines = args.lines_to_print,
		.colour = cli_colour,
		.confirm = cli_confirm,
		.multi_confirm = cli_multi_confirm,
		.input = cli_input,
	};

	struct tsk_items items = {};
	arr_reserve (&items, 0xfff);

	args.to->prog_name = program_invocation_short_name;

	tsk_ctx tsk_ctx;
	if (tsk_update_ctx (&tsk_ctx, true, &pc, args.to))
		exit (1);

	switch (args.mode) {
	case PM_LIST_SHORTCUTS: list_shortcuts (args.to->data_dir); exit (0); break;
	case PM_COMPLETE:       complete (&tsk_ctx, args.str_for_completion); exit (0); break;
	case PM_NORMAL:         break;
	}

	tsk_cmd cmd = tsk_parse_cmd (&tsk_ctx, args.cmd_words.d, args.cmd_words.n,
			CMD_SOURCE_FRONTEND, nullptr);
	if (cmd.flags & TCF_FAILED)
		exit (1);

	if (args.mode == PM_COMPLETE)
		exit (0);

	if (args.tui_mode == COM_ALWAYS ||
			(args.tui_mode == COM_AUTO && isatty (STDOUT_FILENO)))
		run_tui (&tsk_ctx, &cmd, args.to, args.colour_mode, args.lines_to_print,
				args.cols);

	bool should_write = false;
	bool notes_were_updated;

	int rt = tsk_do_cmd (&tsk_ctx, &cmd, &notes_were_updated);
	if (rt)
		exit (1);

	should_write |= notes_were_updated;
	if (!(args.to->flags & TSK_FLAG_NO_AUTOCMD) && notes_were_updated)
		tsk_run_autocmds (&tsk_ctx, &cmd, &notes_were_updated);

	print_items (&tsk_ctx, &cmd);

	should_write |= notes_were_updated;
	if (should_write)
		ASSERT (!tsk_write (&tsk_ctx, &tsk_ctx.items, &cmd));

	exit (0);
}

