#pragma once

#include "../str.h"

enum longargs {
	LO_DATA_DIR = 1000,
	LO_COMPLETE,
	LO_NO_ALWAYS_PRINT_CMD,
	LO_LIST_SHORTCUTS,
	LO_LONG,
	LO_IMPLICIT_LIST,
	LO_NORMAL,
};

enum colour_mode {
	COLOUR_MODE_NEVER,
	COLOUR_MODE_ALWAYS,
	COLOUR_MODE_AUTO,
};

enum prog_mode {
	PM_NORMAL,
	PM_LIST_SHORTCUTS,
	PM_COMPLETE,
};

enum opt_mode {
	COM_AUTO,
	COM_NEVER,
	COM_ALWAYS,
};

struct stat_arr {
	struct stat *d;
	size_t n, a;
};

typedef struct args args;
struct args {

	// This gets malloced in args.c. Fuck knows why it needs to be.
	// Before I was passing it by reference to tsk_parse_cmds, and it
	// worked for many years. Now, on this computer, I get garbage
	// values.
	struct tsk_options *to;
	struct str_arr cmd_words;

	enum opt_mode tui_mode;
	enum prog_mode mode;
	enum colour_mode colour_mode;
	int lines_to_print;
	int cols;
	struct stat_arr notefile_stats;

	// This is the string used for coimpltion. It's a string so I can
	// work out where
	char *str_for_completion;
};

args make_args (int argc, char **argv);
int parse_args (int argc, char **argv, bool from_tui, args *args);
