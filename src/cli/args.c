#define _GNU_SOURCE

#include "../../config.h"
#include "../macros.h"
#include "../sys.h"
#include "args.h"
#include "config.h"
#include <argp.h>
#include <err.h>
#include <libnote/libnote.h>
#include <n-fs.h>
#include <unistd.h>
#include <wrap.h>

str_arr_s NEW_SFILE_BASEDIRS; // No suffix, either.

enum doing {
	DOING_NOTHING,
	DOING_LIST,
	DOING_ACTION,
};

struct args_and_cmd {
	struct args *args;
	struct tsk_items *cmds;
};

char *GLOBAL_DATA_DIR;
int CMD_I;

#define args_int_err(name, ...) \
	errx (1, name " is not a valid int for %s", #__VA_ARGS__)

static int get_ls_color_style_val_or_die (char *arg, char *arg_name) {
	if (!arg)
		return 2;
	int match = get_index_of_longest_unambiguous_initial_match (arg,
			(char *[]) {"auto", "never", "always"}, 3);
	if (-1 == match)
		errx (1, "\
%s \"%s\" isn't unambiguiously \"auto\", \"never\" or \"always\"",
arg_name, arg);
	return match;
}

static bool should_print_colour (enum colour_mode colour_mode) {
	if (colour_mode == COLOUR_MODE_ALWAYS)
		return 1;
	else if (colour_mode == COLOUR_MODE_NEVER)
		return 0;
	else if (isatty (STDOUT_FILENO))
		return 1;
	return 0;
}

static error_t parse_arg (int key, char *arg, [[maybe_unused]] struct argp_state *state) {

	args *args = state->input;

	static char err_buf[BUFSIZ];
	switch (key) {
		case LO_NO_ALWAYS_PRINT_CMD:
			args->to->flags &= ~TSK_FLAG_ALWAYS_PRINT_CMD;
			break;

		case LO_IMPLICIT_LIST:
			args->to->flags |= TSK_FLAG_IMPLICIT_LIST;
			break;

		case 'C':
			args->colour_mode = get_ls_color_style_val_or_die (arg, "--colour");
			break;

		case 'n':
			if (GET_INT (&args->to->nnewlines_between_notes, arg, 0))
				args_int_err ("--newlines");
			if (args->to->nnewlines_between_notes < 0)
				errx (1, "You've done a negative --newlines");
			break;

		case 's':
			args->to->flags |= TSK_FLAG_PRINT_SHORT;
			break;

		case 'p':
			tsk_get_print_what (&args->to->print_what, arg, err_buf);
			break;

		case 'f':
			args->to->confirmation_mode = CM_FORCE;
			break;

		case 'c':
			args->to->confirmation_mode = CM_CONFIRM;
			break;

		case LO_NORMAL:
			args->to->confirmation_mode = CM_NORMAL;
			break;

		case 'T':
			if (GET_INT (&args->to->truncate_len, arg, 0))
				err (1, "\"%s\" isn't a valid integer", arg);
			break;

		case 'A':
			args->to->flags |= TSK_FLAG_NO_AUTOCMD;
			break;

		case 'S':
			args->to->flags |= TSK_FLAG_SORT;
			break;
		case LO_COMPLETE:
			args->mode = PM_COMPLETE;
			args->str_for_completion = arg;
			break;

		case LO_LIST_SHORTCUTS:
			args->mode = PM_LIST_SHORTCUTS;
			break;

		case 'w':

			if (GET_INT (&args->cols, arg, 0))
				args_int_err ("--width");
			if (args->to->nnewlines_between_notes < 0)
				errx (1, "You've done a negative --width");
			break;

		case LO_LONG:
			args->to->flags &= ~TSK_FLAG_PRINT_SHORT;
			break;

		case 'L':
			if (GET_INT (&args->lines_to_print, arg, 0))
				args_int_err ("--lines");
			break;

		case 'N':
			arr_add (&NEW_SFILE_BASEDIRS, arg);
			break;

		case 't':
			args->tui_mode = get_ls_color_style_val_or_die (arg, "--tui");
			break;

		case LO_DATA_DIR:
			args->to->data_dir = arg;
			break;

		case ARGP_KEY_INIT:
			args->cmd_words.n = 0;
			break;

		case ARGP_KEY_ARG:
			arr_add (&args->cmd_words, strdup (arg));
			break;

		case ARGP_KEY_END:

			if (args->lines_to_print == -1) args->lines_to_print = 0;
			if (should_print_colour (args->colour_mode))
				args->to->flags |=  TSK_FLAG_USE_COLOUR;

			// There'll be an extraneous space in args.cmd_str.
			args->to->data_dir = args->to->data_dir ?: n_fs_get_full_path (DEFAULT_DATA_DIR);

			// This is writing new ctx->sfiles from basenames collected
			// from -N. FIXME: this is wrong. The user of the library
			// shouldn't be doing this.
			char buf[PATH_MAX];
			if (NEW_SFILE_BASEDIRS.n) {
				EACH (basedir, NEW_SFILE_BASEDIRS.n, NEW_SFILE_BASEDIRS.d) {
					snprintf (buf, PATH_MAX, "%s/%s.nxt", args->to->data_dir, *basedir);
					FILE *f = fopen (buf, "w");
					if (!f)
						errx (1, "Can't open %s", buf);
					fprintf (f, "\n");
				}

				// Just exit. I could continue and let you use the
				// thing right away (as I promised confidently in the
				// git commit) but, trying it out, I can see you'd
				// forget to write swhatever afterwards.
				exit (0);
			}

			// Get the paths of all the ctx->sfiles. Used for match
			// later.
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

struct argp argp = {
	(struct argp_option []) {
	{"no-always-print-cmd", LO_NO_ALWAYS_PRINT_CMD,
		0, 0,
		"Don't print \"because of ...\" for non-autocmds"},
	{"colour", 'C',
		"WHEN", OPTION_ARG_OPTIONAL,
		"\
When to use colour. Same rules as grep.  \"auto\", (default) \"never\", \"always\" \
(no arg = this). \
colour accepted"},
		{"color", 'C',
			"WHEN", OPTION_ALIAS},
		{"implicit-list", LO_IMPLICIT_LIST, 0, 0, "\
			Make each statement always start with the previous list's list."},
		{"width", 'w',
			"COLS", 0,
			"Number of columns to wrap output to"},
		{"newlines", 'n',
			"N", 0, "The number of newlines to put between each note"},
		{"short", 's',
			0, 0, "Summarise annotations etc instead of printing them"},
		{"sort", 'S',
			0, 0, "Sort notes by ON"},
		{"no-autocmd", 'A',
			0, 0, "Don't run autocmds"},
		{"shortcuts", LO_LIST_SHORTCUTS,
			0, 0,
			"List SHORTCUT ctx->notefiles"},
		{"shortcut-dir", LO_DATA_DIR,
			"PATH", 0, "Use this directory for your shortcut notefile"},
		{"complete", LO_COMPLETE,
			"NOTE CMDLINE", 0, "Output completions for the last word"},
		{"lines", 'L',
			"INTEGER", 0,
			"Number of lines to print (from each section)"},
		{"new-sfile", 'N',
			"NAME", 0, "\
				Write an empty notefile to data-dir and exit"},
	{"tui", 't',
		"WHEN", OPTION_ARG_OPTIONAL,
		"When to use t: \"auto\" (default), \"never\" and \"always\" \
			(no arg = this)"},
		{"truncate-at", 'T',
			"INT", 0,
"The number of chars after which certain strings should be truncated. \
It's used wherever something from a cmd or item is printed out"},
			{"confirm", 'c', 0, 0, "Like :confirm but for each statement"},
			{"force", 'f', 0, 0, "Like :force but for each statement"},
			{"normal", LO_NORMAL, 0, 0, "Like :normal but for each statement"},
			{"print", 'p', "PROP LIST", 0, "Like :print but for each statement"},
			{"long", LO_LONG, 0, 0, "Print notes instead of summarising them. Default"},
			{},
}, parse_arg, "[LIST] [ACTION]", "\
Make a NOTE or perform an ACTION on a LIST of notes.\
\v\
A file in ~/.local/share/note/initial.nxt if present is always read in \
first. You can put autocmds in it. Perhaps you want all notes to be \
locked.\n\
\n\
--verbosity and --autocmd-verbosity are numbers from 1 to 4:\n\
0: Nothing\n\
1: Errors only\n\
2: Errors, changes\n\
3: Errors, changes, match failures\n\
4: Errors, changes, match failures, non-changes\n\
"};

// This function can get called from the TUI so it does stuff, like
// the --list-shortcuts option.
int parse_args (int argc, char **argv, bool from_tui, args *args) {

	// Probably wrap's your_default_wrap_opts should actually be a
	// pointer, and it should be stored in args. Because the wrap opts
	// is something the user might want to change, meaning it should
	// be part of args.
	your_default_wrap_opts = (struct wrap_opts) {
		.tab_size = 4, .cols = args->cols == -1
			? get_terminal_cols ()
			: args->cols
	};
	if (your_default_wrap_opts.cols > PRINT_MAX_COLS)
		your_default_wrap_opts.cols = PRINT_MAX_COLS;
	return argp_parse (&argp, argc, argv, (from_tui ? ARGP_NO_EXIT : 0) |

			// Passing ARGP_PARSE_ARGV0 doesn't actually help. I make
			// it work by passing inserting a string in the element
			// of argv.
			(from_tui ? ARGP_PARSE_ARGV0 : 0), 0, args);
}

args make_args (int argc, char **argv) {

	args r = {
		.cols = -1,
		.lines_to_print = -1,
		.tui_mode = COM_AUTO,
	};
	r.to = CALLOC_1 (sizeof *r.to);
	r.to->flags = TSK_FLAG_ALWAYS_PRINT_CMD;
	r.to->nnewlines_between_notes = NNEWLINES_BETWEEN_NOTES;

	r.colour_mode = COLOUR_MODE_AUTO;

	if (parse_args (argc, argv, 0, &r))
		exit (1);
	return r;
}




