#define _GNU_SOURCE
#include "sys.h"
#include <rab.h>
#include <darr.h>
#include <readlinef.h>
#include <readlinef.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fnmatch.h>
#include <err.h>
#include <dirent.h>

// This is the third "get files in dir" function I have. The other two
// are embarassing travesties. This one will at least be simpler. I
// require a one-dir-read function to rule them all.
int get_files_in_dir (char *glob_pattern, str_arr_s *buf, char *dir,
		bool full_paths, char *err_buf, size_t n_err_buf) {
	int r = 0;
	char *save_dir = 0;

	// Just init it. Don't pass initialised buf.
	arr_init (buf);
	if (!full_paths) {
		save_dir = getcwd (0, 0);
		if (chdir (save_dir)) {
			snprintf (err_buf, n_err_buf, "Couldn't cd to %s", dir);
			r = -1;
			goto out;
		}
	}
	DIR *d = opendir (dir);
	if (!d)
		err (1, "%s", dir);
	struct dirent *dirent;
	while ((dirent = readdir (d))) {
		if (!!strcmp (dirent->d_name, ".")
				&& !!strcmp (dirent->d_name, "..")
				&& !fnmatch (glob_pattern, dirent->d_name, 0))
		arr_add (buf, strdup (dirent->d_name));
	}
out:
	if (!full_paths) {
		if (chdir (save_dir)) {
			snprintf (err_buf, n_err_buf, "Couldn't CD back to %s", dir);
			r = -1;
		}
	}
	if (save_dir) free (save_dir);
	return r;
}

char *get_editor_or_ask (void) {
	char *editor = getenv ("VISUAL");
	if (editor)
		return editor;

	editor = getenv ("EDITOR");
	if (editor)
		return editor;
	editor = readlinef (1, "EDITOR and VISUAL not set. Enter editor to use: ");
	strip (editor);
	return editor;
}

int get_terminal_cols (void) {
	struct winsize w;
	ioctl (0, TIOCGWINSZ, &w);
	return w.ws_col;
}

