#pragma once

#include <assert.h>
#define auto __auto_type
#define countof($x) ((ssize_t) ((sizeof($x)/sizeof(($x)[0])) / ((ssize_t)(!(sizeof($x) % sizeof(($x)[0]))))))
#define ASSERT assert
#define EACH($item, $n, $arr) \
	for (typeof (($arr)[0]) *($item) = ($arr); ($item) < ($arr) + ($n); ($item)++)
#define FORI($idx, $max) for (int $idx = 0; $idx < $max; $idx++)
#define UPDATE_MAX($to_update, $new_val) do { \
	*($to_update) = MAX (*($to_update), $new_val); \
} while (0)
#define MAX($a, $b)  \
	({ __typeof__ ($a) a = ($a); \
	 __typeof__ ($b) b = ($b); \
	 a > b? a : b; })
#define MIN($a, $b) \
	({ __typeof__ ($a) a = ($a); \
	 __typeof__ ($b) b = ($b); \
	 a > b ? b : a; })
#define GET_INT(_res, str, e) 															\
	({ 																					\
		char *end; 																		\
		errno = 0; 																		\
		*_res = strtol (str, &end, 0); 													\
		/* If you've provided e, it's an error if you strtof doesn't parse */ 			\
		/* to e. If not, it's an error if you didn't parse anything */ 					\
		errno || (e? end != e: end == str)? 1: 0; 										\
	})
#define CALLOC_1($size) calloc (1, $size)
#define WARN_GOTO_OUT($fmt, ...) \
	do { \
		fprintf (stderr, $fmt __VA_OPT__(,) __VA_ARGS__); \
		goto out; \
	} while (0)
#define WARN_RETURN($val, $fmt, ...) \
	do { \
		fprintf (stderr, $fmt __VA_OPT__(,) __VA_ARGS__); \
		return $val; \
	} while (0)
#define np nullptr
#define $plural(x) ((x) == 1 ? "" : "s")
