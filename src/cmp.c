#define _GNU_SOURCE
#include "cmp.h"

int cmp_int_cb (const void *a, const void *b) {
	return *(int *) a - *(int *) b;
}
